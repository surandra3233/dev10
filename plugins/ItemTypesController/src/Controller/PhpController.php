<?php
namespace ItemTypesController\Controller;

use ItemTypesController\Controller\AppController;

/**
 * Php Controller
 *
 * @property \ItemTypesController\Model\Table\PhpTable $Php
 */
class PhpController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $php = $this->paginate($this->Php);

        $this->set(compact('php'));
        $this->set('_serialize', ['php']);
    }

    /**
     * View method
     *
     * @param string|null $id Php id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $php = $this->Php->get($id, [
            'contain' => []
        ]);

        $this->set('php', $php);
        $this->set('_serialize', ['php']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $php = $this->Php->newEntity();
        if ($this->request->is('post')) {
            $php = $this->Php->patchEntity($php, $this->request->data);
            if ($this->Php->save($php)) {
                $this->Flash->success(__('The php has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The php could not be saved. Please, try again.'));
        }
        $this->set(compact('php'));
        $this->set('_serialize', ['php']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Php id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $php = $this->Php->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $php = $this->Php->patchEntity($php, $this->request->data);
            if ($this->Php->save($php)) {
                $this->Flash->success(__('The php has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The php could not be saved. Please, try again.'));
        }
        $this->set(compact('php'));
        $this->set('_serialize', ['php']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Php id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $php = $this->Php->get($id);
        if ($this->Php->delete($php)) {
            $this->Flash->success(__('The php has been deleted.'));
        } else {
            $this->Flash->error(__('The php could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
