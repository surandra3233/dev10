<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectBudgetsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectBudgetsTable Test Case
 */
class ProjectBudgetsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectBudgetsTable
     */
    public $ProjectBudgets;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.project_budgets',
        'app.projects',
        'app.cities',
        'app.states',
        'app.countries',
        'app.locations',
        'app.users',
        'app.roles',
        'app.project_users',
        'app.projectbudgets',
        'app.clients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProjectBudgets') ? [] : ['className' => 'App\Model\Table\ProjectBudgetsTable'];
        $this->ProjectBudgets = TableRegistry::get('ProjectBudgets', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProjectBudgets);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
