<?php
namespace App\Test\TestCase\Controller;

use App\Controller\LedgersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\LedgersController Test Case
 */
class LedgersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ledgers',
        'app.projects',
        'app.cities',
        'app.states',
        'app.countries',
        'app.locations',
        'app.users',
        'app.roles',
        'app.project_users',
        'app.projectbudgets',
        'app.clients',
        'app.transaction_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
