<?php


namespace App\Log\Engine;
use Cake\Log\Engine\BaseLog;
use Cake\ORM\TableRegistry;
class AccessLog extends BaseLog
{
    public function __construct($options = []){
	//debug($options);die;
        parent::__construct($options);
      
  //  $this->AccessLog = new AccessLog;  
     $this->CurrentModel=TableRegistry::get($options['model']);
      
     //  debug($this->CurrentModel);die;
    }

    public function log($level, $message, array $context = []){
		//debug($this->CurrentModel);die;
		
		 $this->CurrentModel->write($level, $message ,$context);
    }
}

