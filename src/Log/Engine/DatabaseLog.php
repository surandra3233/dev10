<?php


namespace App\Log\Engine;
use Cake\Log\Engine\BaseLog;
use Cake\ORM\TableRegistry;
class DatabaseLog extends BaseLog
{
    public function __construct($options = []){
	//	debug($options);die;
        parent::__construct($options);
        $this->CurrentModel=TableRegistry::get($options['model']);
        
    }

    public function log($level, $message, array $context = []){
		//debug($context);die;
		 $this->CurrentModel->addLog($level, $message,$context);
    }
}

