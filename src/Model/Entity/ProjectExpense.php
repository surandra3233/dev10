<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProjectExpense Entity
 *
 * @property int $id
 * @property int $project_id
 * @property int $item_id
 * @property string $quantity
 * @property int $price_per_unit
 * @property int $tax_amount
 * @property int $total_price
 * @property \Cake\I18n\Time $added_on
 * @property int $user_id
 * @property int $ledger_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\Item $item
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Ledger $ledger
 */
class ProjectExpense extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
