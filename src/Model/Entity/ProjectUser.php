<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProjectUser Entity
 *
 * @property int $project_id
 * @property int $user_id
 *
 * @property \App\Model\Entity\Project $project
 * @property \App\Model\Entity\User $user
 */
class ProjectUser extends Entity
{

}
