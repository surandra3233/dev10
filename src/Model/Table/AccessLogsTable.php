<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use  App\Controller\AuthComponent;

class AccessLogsTable extends Table{
	 public function initialize(array $config){
        $this->addBehavior('Timestamp');
        
    }
 
    
   public function write($level, $message , $context) {
		//debug($context);die;
        $access_logs['type'] = ucfirst($level);
        $access_logs['time'] = date('Y-m-d H:i:s');
        $access_logs['message'] = $message;
        $access_logs['user_id']=$context['user_id'];
        $access_logs['user_name']=$context['username'];
        $Obj=$this->newEntity($access_logs);
		$this->save($Obj);
       
    }
    public function beforeSave(Event $event, EntityInterface $entity) {	
		$entity['ip'] = env('REMOTE_ADDR');		
		$entity['hostname'] = env('HTTP_HOST');		
		$entity['uri'] = env('REQUEST_URI');		
		$entity['refer'] = env('HTTP_REFERER');		
		$entity['user_agent'] = env('HTTP_USER_AGENT');	
	}
	
}

