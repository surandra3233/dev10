<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsTo $States
 * @property \Cake\ORM\Association\BelongsTo $Countries
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('projects');
        $this->displayField('project_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        
        
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
            $this->hasMany('ProjectUsers', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
           
        ]);
            $this->hasMany('Projectbudgets', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER',
           
        ]);
        
      /*  $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
           
        ]);*/
         
       $this->belongsTo('Clients', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className'=>'Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id', 'ID is required.')
            ->allowEmpty('id', 'create', 'ID cannot be left empty.');

        $validator
            ->requirePresence('project_name', 'create', 'First name is required.')
            ->notEmpty('project_name','First name cannot be left empty.')
            ->add('project_name', 'unique', [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This Project name is already registered. Please add another project name.'
                     ]);

          $validator
            ->requirePresence('manager', 'create', 'Manager is required.')
            ->notEmpty('manager','Manager cannot be left empty.');
            
         $validator
                ->requirePresence('pincode', 'create','Pincode is required .')
                ->notEmpty('pincode','Pincode cannot be left empty .')
                ->numeric('pincode','Please, enter valid pincode .') 
                ->add('pincode', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The pincode have to be at least 6 digits.',
                    ]
                ])
                ->add('pincode', [
                    'length' => [
                        'rule' => ['maxLength', 6],
                        'message' => 'The pincode have to be maximum 6 digits.',
                    ]
                ]);
       $validator
            ->requirePresence('started_on', 'create', 'Starting date  is required.')
            ->notEmpty('started_on','Starting date cannot be left empty.');
        
        $validator
            ->requirePresence('completed_on', 'create', 'Completion date  is required.')
            ->notEmpty('completed_on','Completion date cannot be left empty.');
            
        $validator
            ->requirePresence('estimated_budget', 'create','Estimated budget  is required.')
            ->notEmpty('estimated_budget','Estimated budget cannot be left empty.');
            
             $validator
            ->requirePresence('approved_budget', 'create','Approved budget  is required.')
            ->notEmpty('approved_budget','Approved budget cannot be left empty.');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['projectname']));
       
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
     
        return $rules;
    }
}
