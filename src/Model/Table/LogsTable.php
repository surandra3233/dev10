<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;

class LogsTable extends Table{
	 public function initialize(array $config){
        $this->addBehavior('Timestamp');
    }
	public function addLog($level, $message,$context){
		//debug($level);die;
		$arr=[
			 'type'=>$level,
			 'message'=>$message,
			 'context'=>json_encode($context)
		];
		$Obj=$this->newEntity($arr);
		$this->save($Obj);
	}
	public function beforeSave(Event $event, EntityInterface $entity) {	
		$entity['ip'] = env('REMOTE_ADDR');		
		$entity['hostname'] = env('HTTP_HOST');		
		$entity['uri'] = env('REQUEST_URI');		
		$entity['refer'] = env('HTTP_REFERER');		
		$entity['user_agent'] = env('HTTP_USER_AGENT');	
	}
	
}
