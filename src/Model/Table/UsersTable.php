<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Roles
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsTo $States
 * @property \Cake\ORM\Association\BelongsTo $Countries
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
      
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id', 'ID is required.')
                ->allowEmpty('id', 'create', 'ID cannot be left empty.');

        $validator
                ->requirePresence('first_name', 'create', 'First name is required.')
                ->notEmpty('first_name', 'First name cannot be left empty.');

        $validator
                ->requirePresence('last_name', 'create', 'Last name is required.')
                ->notEmpty('last_name', 'Last name cannot be left empty.');

        $validator
                ->requirePresence('username', 'create', 'Username is required.')
                ->notEmpty('username', 'Username cannot be left empty.')
                 ->add('username', 'unique', [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This username is already registered. Please add another username.'
    ]);


        $validator
                ->email('email', 'Email is required.')
                ->requirePresence('email', 'create', 'Email cannot be left empty.')
                ->notEmpty('email', 'Email is required.')
                ->add('email', 'unique', [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This email is already registered. Please add another email.'
                        ]
        );
        
        $validator
                ->requirePresence('phone', 'create','Phone number is required .')
                ->notEmpty('phone','Phone number cannot be left empty .')
                ->numeric('pincode','Please, enter valid phone number .') 
                ->add('phone', [
                    'length' => [
                        'rule' => ['minLength', 10],
                        'message' => 'The phone number have to be at least 10 digits.',
                    ]
                ])
                ->add('phone', [
                    'length' => [
                        'rule' => ['maxLength', 10],
                        'message' => 'The phone number have to be maximum 10 digits.',
                    ]
                ]);

        $validator
                ->requirePresence('password', 'create','Password is requires .')
                ->notEmpty('password','Password cannot be left empty .');

        $validator
                ->requirePresence('pincode', 'create','Pincode is required .')
                ->notEmpty('pincode','Pincode cannot be left empty .')
                ->numeric('pincode','Please, enter valid pincode .') 
                ->add('pincode', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The pincode have to be at least 6 digits.',
                    ]
                ])
                ->add('pincode', [
                    'length' => [
                        'rule' => ['maxLength', 6],
                        'message' => 'The pincode have to be maximum 6 digits.',
                    ]
                ]);

        $validator
                ->dateTime('last_login')
                ->allowEmpty('last_login');

        return $validator;
    }

    public function validationeditdetailsUsers(Validator $validator) {
 $validator
                ->integer('id', 'ID is required.')
                ->allowEmpty('id', 'create', 'ID cannot be left empty.');

        $validator
                ->requirePresence('first_name', 'create', 'First name is required.')
                ->notEmpty('first_name', 'First name cannot be left empty.');

        $validator
                ->requirePresence('last_name', 'create', 'Last name is required.')
                ->notEmpty('last_name', 'Last name cannot be left empty.');

        $validator
                ->requirePresence('username', 'create', 'Username is required.')
                ->notEmpty('username', 'Username cannot be left empty.')
                  ->add('username', 'unique', [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This username is already registered. Please add another username.'
    ]);

        $validator
                ->email('email', 'Email is required.')
                ->requirePresence('email', 'create', 'Email cannot be left empty.')
                ->notEmpty('email', 'Email is required.')
                ->add('email', 'unique', [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'This email is already registered. Please add another email.'
                        ]
        );
        
        $validator
                ->requirePresence('phone', 'create','Phone number is required .')
                ->notEmpty('phone','Phone number cannot be left empty .')
                ->numeric('pincode','Please, enter valid phone number .') 
                ->add('phone', [
                    'length' => [
                        'rule' => ['minLength', 10],
                        'message' => 'The phone number have to be at least 10 digits.',
                    ]
                ])
                ->add('phone', [
                    'length' => [
                        'rule' => ['maxLength', 10],
                        'message' => 'The phone number have to be maximum 10 digits.',
                    ]
                ]);

        $validator
               ->allowEmpty('password', 'create');

        $validator
                ->requirePresence('pincode', 'create','Pincode is required .')
                ->notEmpty('pincode','Pincode cannot be left empty .')
                ->numeric('pincode','Please, enter valid pincode .') 
                ->add('pincode', [
                    'length' => [
                        'rule' => ['minLength', 6],
                        'message' => 'The pincode have to be at least 6 digits.',
                    ]
                ])
                ->add('pincode', [
                    'length' => [
                        'rule' => ['maxLength', 6],
                        'message' => 'The pincode have to be maximum 6 digits.',
                    ]
                ]);

        $validator
                ->dateTime('last_login')
                ->allowEmpty('last_login');

        return $validator;
    }

    public function validationchangePassword(Validator $validator) {



        $validator->requirePresence('cpassword', function ($context) {
            if (isset($context['data']['cpassword'])) {
                return $context['data']['cpassword'];
            }
            return false;
        });
        $validator
                ->notEmpty('cpassword', 'Password cannot be left empty.')
                ->add('cpassword', 'custom', [
                    'rule' => function($value, $context) {
                        $user = $this->get($context['data']['id']);
                         if ($user) {
                            if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                        return false;
                    },
                    'message' => 'Password Incorrect. Please Enter The correct Password'
        ]);

        $validator
                ->requirePresence('npassword')
                ->notEmpty('npassword', 'Password cannot be left empty.');

        $validator
                ->requirePresence('passwordconfirm')
                ->notEmpty('passwordconfirm', 'Re Enter Your Password please.')
                ->add('passwordconfirm', [
                    'match' => [
                        'rule' => ['compareWith', 'npassword'],
                        'message' => 'The passwords does not match!',
                    ]
                        ]
        );




        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
      

        return $rules;
    }

}
