<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TransactionTypes Model
 *
 * @property \Cake\ORM\Association\HasMany $Ledgers
 *
 * @method \App\Model\Entity\TransactionType get($primaryKey, $options = [])
 * @method \App\Model\Entity\TransactionType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TransactionType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TransactionType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TransactionType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TransactionType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TransactionType findOrCreate($search, callable $callback = null, $options = [])
 */
class TransactionTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('transaction_types');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Ledgers', [
            'foreignKey' => 'transaction_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('transaction_type_name', 'create')
            ->notEmpty('transaction_type_name');

        return $validator;
    }
}
