<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
           Credits
        </li>
        <li class="active">Edit Credit</li>
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cc-mastercard"></i>Edit Credits </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->

        <?php echo $this->Form->create($payment, array('class' => 'form-horizontal', 'id' => 'edit')); ?>



        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">Client Name</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->hidden('id'); 
                    echo $this->Form->input('user_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $clients,
                        //'onChange' => "getproject();"
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Project Name</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('project_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $projects
                    ));
                    ?>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-3 control-label">Amount</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('amount', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            
                   <div class="form-group">
                <label class="col-md-3 control-label">Date</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('date', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'id' => 'datepicker1',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            
                       <div class="form-group">
                <label class="col-md-3 control-label">Refrence No/Bank No</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('refrence_no', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'id' => 'refrence_no',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Remarks</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('remarks', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'id' => 'remarks',
                        'required' => true,
                        'type' => 'textarea'
                    ));
                    ?>
                </div>
            </div>
            
           <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>



</div>
<?php echo $this->Html->script(['jquery.validate.min.js', 'bootstrap-datepicker.min.js']); ?>
<?php echo $this->Html->css(['bootstrap-datepicker3.min.css']); ?>
<script>
	
	$('document').ready(function ()
    {   
        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            startDate:'01-01-2013'
          });
          
          
             $("#edit").validate({
            rules: {
                user_id: {
                    required: true,
                },
                project_id: {
                    required: true,
                },
                amount: {
                    required: true,
                    validamount:true
                },
                date: {
                    required: true,
                    
                },
                refrence_no: {
                    required: false,
                    validnumber:true
                   
                },
                remarks: {
                    required: false
                   
                },
                
            },
            messages: {
                user_id: {required: "Please select client name."
                },
                project_id: {required: "Please select project name.."
                },
                amount: {
                    required: 'Please enter amount'
                },
                date: {
                    required: 'Please enter date.'
                },
        
            }
        });
     });
     
     
       jQuery.validator.addMethod("validnumber", function (value, element) {
        return this.optional(element) || /^[0-9]+$/.test(value);
       }, "Only Numbers are allowed");
       
       jQuery.validator.addMethod("validamount", function (value, element) {
        return this.optional(element) || /^[0-9.]+$/.test(value);
    }, "Only  Integer Or Decimal Numbers are allowed");
     
     
       $('#submitbutton').click(function () {

        if ($("#edit").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Payments', 'action' => 'editDetails']); ?>",
                data: $("#edit").serialize(),
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
                        $('#submitbutton').attr('disabled', 'disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
  
</script>
