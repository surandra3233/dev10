<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Payments
        </li>
        <li class="active">All Payments</li>
    </ol>
</div>
<?php //debug($id); ?>



<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
           <i class="fa fa-bars" aria-hidden="true"></i> All Payments </div>
                            <div class="actions">

               <a href="javascript:;" class="btn blue-madison" onClick="viewBalance(<?php  echo $id ?>);">
                    <i class="fa fa-plus"></i> Payments Detail</a>
            </div>
       
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-advance " id="payments" style="width:100%;">
                <thead>
                          <tr>

                            <th><?php echo __('Client Name'); ?></th>
                            <th><?php echo __('Project  Name'); ?></th>
                           <th><?php echo __('Amount'); ?></th>
                           <th><?php echo __('Date'); ?></th>
                           <th><?php echo __('Refrence Number/Bank No'); ?></th>
                           
                        </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan='5' align='center'> <?php echo __('Loading'); ?> </td>
                    </tr>
                </tbody>
            </table>
        </div>	
    </div> 
   
    
</div>
<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>


<script>
    jQuery('#document').ready(function ($) {
      projectID='<?php echo $id ?>';
	 // alert(projectID);
      allpayments(projectID);

    });
    
    
    function allpayments(projectID) {

        $('#payments').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                   
                }
            ],
        "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Payments', 'action' => 'getPayments']); ?>/"+projectID,
        });
    }
    
    function viewBalance(id) {
      $('#SuccessDiv1').hide();
      $('#ErrorDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Payments', 'action' => 'paymentDetail']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    
 </script>
