<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <!--<h1>All Users</h1>-->
    <ol class="breadcrumb">
        <li>
            Credits
        </li>
        <li class="active">All Credits </li>
    </ol>
</div>

<div class="col-md-12">
    <?php echo $this->Flash->render(); ?>

    <div class="portlet portlet box red" > 
        <div class="portlet-title">
            <div class="caption mil1">
                <i class="fa fa-users"></i><?php echo __('All Credits') ?> </div>

            <div class="actions">

                <a href="javascript:;" class="btn blue-madison" onClick="addPayment();">
                    <i class="fa fa-plus"></i> Add New Credit</a>
            </div>      



        </div>
       
        <div class="portlet-body">
			 <div class='row'>				
                 <div class="col-md-4">
					<?php echo $this->Form->input('project_id',array('options'=>$projects,'label'=>false,'class'=>'form-control','empty'=>'All','id'=>'project_id',  'onchange'=>"setFilter()")); ?>
                 </div>
             </div> <br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-advance " id="payments">
                    <thead>
                        <tr>

                            <th><?php echo __('Client Name'); ?></th>
                            <th><?php echo __('Project  Name'); ?></th>
                           <th><?php echo __('Amount'); ?></th>
                           <th><?php echo __('Date'); ?></th>
                           <th><?php echo __('Refrence Number/Bank No'); ?></th>
                           <th><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='6' align='center'> <?php echo __('Loading'); ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>	
        </div>
    </div>    
</div>
<div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="details">

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>


<script>
    jQuery('#document').ready(function ($) {
       // allpayments();
        allpayments($('#project_id').val());

    });
    
    function setFilter(){
		var project_id=$('#project_id').val();
		//alert(project_id);die;
		 allpayments($('#project_id').val());
    }
    function allpayments(project_id=null) {

        $('#payments').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
            {
                    "render": function (data, type, row) {
						if(row.refrence_no){
							return row.refrence_no;
						}else{
							str='NULL';
							return str;
						}
                    },
                    "targets": 4
                },
                {
                    "targets": 5,
                    "data": function (row, type, val, meta) {
                    
                       str = "&nbsp;<a class='label label-sm label-danger' href='javascript:;' onclick=openEditModal(" + row[5] + ")>Edit</a>";
                 
                       return str;


                    }
                }
            ],
        "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Payments', 'action' => 'index1']); ?>?projectId=" +project_id,
        });
    }
    
      function addPayment() {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Payments', 'action' => 'add']); ?>",
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });

    }
    
    
     function openEditModal(id) {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Payments', 'action' => 'edit']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    
       
</script>
