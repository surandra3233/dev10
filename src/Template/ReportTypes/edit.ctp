<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $reportType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $reportType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Report Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Reports'), ['controller' => 'Reports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Report'), ['controller' => 'Reports', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="reportTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($reportType) ?>
    <fieldset>
        <legend><?= __('Edit Report Type') ?></legend>
        <?php
            echo $this->Form->input('report_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
