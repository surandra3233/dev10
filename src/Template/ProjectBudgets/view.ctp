<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Project Budget'), ['action' => 'edit', $projectBudget->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Project Budget'), ['action' => 'delete', $projectBudget->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectBudget->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Project Budgets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project Budget'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="projectBudgets view large-9 medium-8 columns content">
    <h3><?= h($projectBudget->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Project') ?></th>
            <td><?= $projectBudget->has('project') ? $this->Html->link($projectBudget->project->projectname, ['controller' => 'Projects', 'action' => 'view', $projectBudget->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($projectBudget->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alloted By') ?></th>
            <td><?= $this->Number->format($projectBudget->alloted_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($projectBudget->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alloted On') ?></th>
            <td><?= h($projectBudget->alloted_on) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($projectBudget->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($projectBudget->modified) ?></td>
        </tr>
    </table>
</div>
