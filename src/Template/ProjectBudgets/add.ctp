<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Project
        </li>
        <li class="active">Add Project Budget</li>
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-inr" aria-hidden="true"></i>Add Project Budget </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?php// debug($projectdata);?>
        <?php foreach ($projectdata as $p): ?>
       
        <?php endforeach; ?>
        
        <?php echo $this->Form->create($projectBudget, array('class' => 'form-horizontal', 'id' => 'add')); ?>


        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">Amount</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->hidden('project_id', array('value' => $id));
                    echo $this->Form->input('amount', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'onblur'=>'roundvalue();',
                        'id'=>'budgetAmount'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Alloted By</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('user_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $users
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Alloted to</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('manager_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $manager
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Alloted On</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('alloted_on', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'id' => 'datepicker1'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>



</div>

<?php echo $this->Html->script(['jquery.validate.min.js', 'bootstrap-datepicker.min.js']); ?>
<?php echo $this->Html->css(['bootstrap-datepicker3.min.css']); ?>

<script>
    jQuery('#document').ready(function ($) {

        $('#datepicker1').datepicker({
			format: 'dd-mm-yyyy',
            autoclose: true,
            startDate:'<?php echo date('d-m-Y',strtotime($p->started_on)); ?>',   
            endDate: '<?php echo date('d-m-Y',strtotime($p->completed_on)); ?>'
        });

        $("#add").validate({
            rules: {
                amount: {
                    required: true,
                    validnumber: true
                },
                alloted_on: {
                    required: true,
                },
            },
            messages: {
                amount: {required: "Please enter the amount."
                },
                alloted_on: {required: "Please enter the the date."
                },
            }
        });
    });

    $('#submitbutton').click(function () {

        if ($("#add").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'ProjectBudgets', 'action' => 'saveDetails']); ?>",
                data: $("#add").serialize(),
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
                        $('#submitbutton').attr('disabled', 'disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
 jQuery.validator.addMethod("validnumber", function (value, element) {
        return this.optional(element) || /^[0-9.]+$/.test(value);
    }, "Only  Integer Or Decimal Numbers are allowed");
    
    function roundvalue(){
       num= $('#budgetAmount').val();
       $('#budgetAmount').val( Math.round(num*100)/100); 
    }
</script>

