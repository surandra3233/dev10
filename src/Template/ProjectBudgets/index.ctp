<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Project Budget'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Projects'), ['controller' => 'Projects', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Project'), ['controller' => 'Projects', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="projectBudgets index large-9 medium-8 columns content">
    <h3><?= __('Project Budgets') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('project_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alloted_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('alloted_on') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($projectBudgets as $projectBudget): ?>
            <tr>
                <td><?= $this->Number->format($projectBudget->id) ?></td>
                <td><?= $projectBudget->has('project') ? $this->Html->link($projectBudget->project->projectname, ['controller' => 'Projects', 'action' => 'view', $projectBudget->project->id]) : '' ?></td>
                <td><?= $this->Number->format($projectBudget->alloted_by) ?></td>
                <td><?= $this->Number->format($projectBudget->amount) ?></td>
                <td><?= h($projectBudget->alloted_on) ?></td>
                <td><?= h($projectBudget->created) ?></td>
                <td><?= h($projectBudget->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $projectBudget->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $projectBudget->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $projectBudget->id], ['confirm' => __('Are you sure you want to delete # {0}?', $projectBudget->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
