<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <!--                            <li class="sidebar-search-wrapper">
                                             BEGIN RESPONSIVE QUICK SEARCH FORM 
                                             DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box 
                                             DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box 
                                            <form class="sidebar-search  sidebar-search-bordered" action="#" method="POST">
                                                <a href="javascript:;" class="remove">
                                                    <i class="icon-close"></i>
                                                </a>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search...">
                                                    <span class="input-group-btn">
                                                        <a href="javascript:;" class="btn submit">
                                                            <i class="icon-magnifier"></i>
                                                        </a>
                                                    </span>
                                                </div>
                                            </form>
                                             END RESPONSIVE QUICK SEARCH FORM 
                                        </li>
            -->
            <?php if ($this->request->session()->read('Auth.User.role_id') == 1): ?>
                <li class="nav-item start ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'dashboard']); ?>" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="arrow"></span>
                    </a>
                </li>

                <li
                    class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Users</span>
                        <span class="arrow"></span>
                    </a>

                </li>

                <li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-area-chart"></i>
                        <span class="title">Projects</span>
                        <span class="arrow"></span>
                    </a>

                </li>
                <li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Taxes', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-indent"></i>
                        <span class="title">Taxes</span>
                        <span class="arrow"></span>
                    </a>

                </li>
                <li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-list" aria-hidden="true"></i>

                        <span class="title">Items</span>
                        <span class="arrow"></span>
                    </a>

                </li>
                <li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>

                        <span class="title">Reports</span>
                        <span class="arrow"></span>
                    </a>

                </li>
                <li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Payments', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                    <i class="fa fa-cc-mastercard" aria-hidden="true"></i>

                        <span class="title">Credits</span>
                        <span class="arrow"></span>
                    </a>

                </li>
                
            <?php elseif ($this->request->session()->read('Auth.User.role_id') == 2): ?>
                <li class="nav-item start ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'managerDashboard']); ?>" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item start ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'managerProject']); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-area-chart"></i>
                        <span class="title">Project</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'manager_index']); ?>" class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>

                        <span class="title">Items</span>
                        <span class="arrow"></span>
                    </a>
               </li>
               
                <li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>

                        <span class="title">Reports</span>
                        <span class="arrow"></span>
                    </a>

                </li>
            <?php else: ?>
                <li class="nav-item start ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'clientDashboard']); ?>" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item start ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'clientProjects']); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-area-chart"></i>
                        <span class="title">Project</span>
                        <span class="arrow"></span>
                    </a>
                </li><li class="nav-item  ">
                    <a href="<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'index']); ?>" class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>

                        <span class="title">Reports</span>
                        <span class="arrow"></span>
                    </a>

                </li>
                
            <?php endif; ?>
        </ul>

    </div>
    <!-- END SIDEBAR -->
</div>
