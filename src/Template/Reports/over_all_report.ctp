<style>
    .raphael-group-91-datalabel text {
        fill: black;
    }
    tspan{
       fill: black; 
    }
</style><div id='target'>
    <div style="text-align: center;">
        <h3>
            <span class="label label-success"> <?php echo "Project Name :- " . $arr['projectName']; ?> </span>
        </h3>
        <h3>
            <span class="label label-danger"> <?php echo "Project Location :- " . $arr['Location']; ?> </span>
        </h3>
    </div>
    <div id="chart-container" style='text-align: center;' class="table-responsive"></div>
</div>
<?php
echo $this->Html->script([
    'fusioncharts.js',
    'fusioncharts.theme.fint.js',
    'fusioncharts-jquery-plugin.js',
    'fusioncharts.charts.js',
    'html2canvas.js',
    'jquery.plugin.html2canvas.js'
]);
?><script>
    $('document').ready(function () {
        FusionCharts.ready(function () {
            visitChart = new FusionCharts({
                type: 'column2d',
                renderAt: 'chart-container',
                width: '700',
                height: '450',
                dataFormat: 'json',
                id: 'revenue-chart',
                dataSource: {
                    "chart": {
                        //"labeldisplay": "rotate",
               // "dateformat": "dd/mm/yyyy",
                        "useellipseswhenoverflow": "1",
                        "caption": "Project Overall Report",
                        "xAxisName": "Project Details",
                        "yAxisName": "Amount",
                        "numberPrefix": "Rs ",
                        "numberSuffix": "",
                        "showAxisLines": "1",
                        "axisLineAlpha": "25",
                        "divLineAlpha": "10",
                        "alignCaptionWithCanvas": "0",
                        "theme": "fint",
                        "exportenabled": "1",
                        "exportatclient": "1",
                        "exporthandler": "https://export.api3.fusioncharts.com",
                        "html5exporthandler": "https://export.api3.fusioncharts.com",
                        //Change bar colors
                        "paletteColors": "#9e88b5 , #c73a6d,#88d0af,#e08468,  #eac536, #3e909e, #e44a00",
                        "usePlotGradientColor": 1,
                        "plotGradientColor": "#ffffff",
                        //Change canval color
                        "canvasBgColor": "#1790e1",
                        //transparency of the canvas background
                        "canvasBgAlpha": "10",
                        //Change cahrt area background color
                        "bgColor": "EEEEEE,CCCCCC",
                        //opaqueness of each color
                        "bgAlpha": "70,80",
                        //contribution of each color to the gradient of the background
                        "bgRatio": "60, 40",
                        //Chart area border
                        "showBorder": "1",
                        //color of the border
                        "borderColor": "#000000",
                        //thickness of the border
                        "borderThickness": "3",
                        "palette": "1"

                    },
   /*  "categories": [
        {
            "category": [
                {
                    "start": "01/09/2017 00:00:00",
                    "end": "05/09/2017 23:59:59",
                    "label": "Monday"
                }
			]
		}
		],*/
                    
                    
                    
                    
                    "data": [
                        {
                            "label": '<?php echo 'Estimated Budget' ?>',
                            "value": <?php echo $arr['estimated_budget'] ?>
                        },
                        {
                            "label": '<?php echo "Approved Budget" ?>',
                            "value": <?php echo $arr['approved_budget'] ?>
                        },
                        {
                            "label": '<?php echo 'Budget Assigned' ?>',
                            "value": <?php echo $arr['budgetAlloted'] ?>
                        },
                        {
                            "label": '<?php echo 'Expense ' ?>',
                            "value": <?php echo $arr['amountSpend'] ?>
                        },
                    ]
                }
            });
            visitChart.render();
            var showChartData = function (data) {
                document.getElementById('chart-data').innerHTML = data.replace(/>/g, '&gt;').replace(/</g, '&lt;').replace(/&gt;&lt;/g, '&gt;<br/>&lt;');
            };
        });
    }
    );
  
