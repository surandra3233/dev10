<div><button class="btn green-meadow" onclick="myFunction();" id="printBtn">Print</button></div>
<div style="text-align: center;">
        <h3>
            <span class="label label-success"> <?php echo "Project Name :- " . $arr['projectName']; ?> </span>
        </h3>
        <h3>
            <span class="label label-danger"> <?php echo "Project Location :- " . $arr['Location']; ?> </span>
        </h3>
    </div>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
    <tr>
        <th>Item</th>
        <th>Quantity</th>
        <th>Price Per unit</th>
        <th>Total amount</th>
        <th>Date</th>
    </tr>
    <?php foreach ($arr['expence'] as $expense) : ?>
        <tr>
            <td>
                <?php echo $expense->item->item_name ?>
            </td>
            <td>
                <?php echo $expense->quantity ?>
            </td>
            <td>
                <?php echo $expense->price_per_unit ?>
            </td>
            <td>
                <?php echo $expense->total_price ?>
            </td>
            <td><?php $d=$this->Time->format( $expense->added_on ,'Y-M-d') ?><?php echo(date("M d, Y", strtotime($d))) ?>
                <?php //echo $expense->added_on ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>
<script>
    function myFunction() {
    window.print();
}
    </script>