<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Project Expenses
        </li>
        <li class="active">All  Project Expenses</li>
    </ol>
</div>
<?php //debug($id); ?>



<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
           <i class="fa fa-bars" aria-hidden="true"></i> All  Project Expenses </div>
                            <div class="actions">

               <a href="javascript:;" class="btn blue-madison" onClick="openAddExpensesModal(<?php  echo $id ?>);">
                    <i class="fa fa-plus"></i> Add Project Expenses</a>
            </div>
       
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-advance " id="projectExpenses" style="width:100%;">
                <thead>
                    <tr>
                        <th><?php echo __('Project Name'); ?></th>
                        <th><?php echo __('Item Type'); ?></th>
                        <th><?php echo __('Item Name'); ?></th>
                        <th><?php echo __('Quantity'); ?></th>
                        <th><?php echo __('Price Per Unit'); ?></th>
                        <th><?php echo __('Tax percentage'); ?></th>
                        <th><?php echo __('Tax Amount'); ?></th>
                        <th><?php echo __('Total Price'); ?></th>
                        <th><?php echo __('Added On'); ?></th>
                        <th><?php echo __('User Name'); ?></th>    
                        <th><?php echo __('Actions'); ?></th> 
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan='10' align='center'> <?php echo __('Loading'); ?> </td>
                    </tr>
                </tbody>
            </table>
        </div>	
    </div> 
   
    
</div>

<div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="details">

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>
<script>
    $('document').ready(function () {
		projectID='<?php echo $id ?>';
		//alert(projectID);
        createExpenseTable(projectID);
       });
     
   function createExpenseTable(projectID) {
        $('#projectExpenses').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'ProjectExpenses', 'action' => 'getProjectExpenses']); ?>/" +projectID,
            
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 10,
                    "data": function (row, type, val, meta) {
						
						
					    var str = "&nbsp;<a class='label label-sm label-info' href='javascript:;' onclick=openExpenseEditModal('" + row[10] + "')>Edit</a>";
						
//						str += "&nbsp;<a class='label label-sm label-warning' href='javascript:;' onclick=openExpenseDeleteModal('" + row[9] + "')>Delete</a>";
						
						str += "&nbsp;<a class='label label-sm label-success'  href='/ProjectExpenses/view1.pdf?ExpenseID=" + row[10] + "'>Generate Invoice</a>"
					
					return str;
						
                    }
                }
            ]
            
        });
    }
     function openExpenseEditModal(id) {
		 projectID='<?php echo $id ?>';
		// alert(projectID);
	    $('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'ProjectExpenses', 'action' => 'edit']); ?>/" + id+'/'+projectID,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    
   function openExpenseDeleteModal(id) {
	    $('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'ProjectExpenses', 'action' => 'delete']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
</script>
