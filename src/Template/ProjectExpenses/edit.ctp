<?php
/**
 * @var \App\View\AppView $this
 */
?>
<style>

    #managerselect{
        width: 100%; 
        overflow: hidden; 
        text-overflow: ellipsis;
    }</style>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Project Expenses
        </li>
        <li class="active">Edit Project Expenses</li>
    </ol>
</div>

<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-area-chart"></i>Edit Project Expenses </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?php //debug($project);?>
        <?php foreach ($project as $p): ?>
         
        <?php endforeach; ?>

        <?php echo $this->Form->create($projectExpenses, array('class' => 'form-horizontal', 'id' => 'edit')); ?>
        <?php //echo $id ?>
        <?php echo $this->Form->hidden('id', array('value' => $id)); ?>
        <?php echo $this->Form->hidden('project_id', array('value' => $projectExpenses->project_id)); ?>


        <div class="form-body">

            <div class="form-group">
                <label class="col-md-3 control-label"> Select Item Type</label>
                <div class="col-md-8">
                    <label class="checkbox-inline">
                        <input type="radio"  id ="selectitems" onClick="openDiv();" name="item_type" checked value="0">&nbsp; Existing Items 
                    </label>
                    <label class="checkbox-inline">
                        <input type="radio"  id ="additems" onClick="openDiv();" name="item_type" value="1">&nbsp;Add New Items
                    </label>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-3 control-label">Item Type</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('item_type_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id'=>'item_type_id',
                        'options' => $itemTypes,
                        'onChange' => "getitems();"
                       
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group" id="div1" >
                <label class="col-md-3 control-label">Items</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('item_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'item_id',
                        'options' => $item,
                        'value' => $projectExpenses->item_id,
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group"  id="div2" style="display:none;">
                <label class="col-md-3 control-label">Add Item</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('add_item', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'id' => 'add_item'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group" id="div3" style="display:none;">
                <label class="col-md-3 control-label">Tax Percentage</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('tax_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'tax_id',
                        'options' => $tax
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Quantity</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('quantity', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'number',
                        'min' => 1,
                        // 'value'=>1,
                        'onChange' => "sum()",
                        'id' => 'quantity'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Price Per Unit</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('price_per_unit', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'value' => 0,
                        'id' => 'price_per_unit',
                        'onChange' => "sum()",
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Tax Amount</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('tax_amount', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'id' => 'tax_amount',
                        'readonly' => 'readonly',
                        'options' => ''
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Total Price</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('total_price', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'readonly' => 'readonly',
                        'id' => 'total_price'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Added On</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('added_on', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'id' => 'datepicker1',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php echo $this->Html->script(['jquery.validate.min.js', 'bootstrap-datepicker.min.js']); ?>
<?php echo $this->Html->css(['bootstrap-datepicker3.min.css']); ?>
<script>
    $('document').ready(function ()
    {
		//  var start='<?php  echo $p->started_on?>';
	    // alert(start); 
         // var today = new Date();
         $('#datepicker1').datepicker({        
            format: 'dd-mm-yyyy',
             autoclose: true,
             startDate:'<?php echo date('d-m-Y',strtotime($p->started_on)); ?>',   
            endDate: '<?php echo date('d-m-Y',strtotime($p->completed_on)); ?>'
            
            
        });
        sum();

        $("#edit").validate({
            rules: {
                add_item: {
                    required: true,
                },
                quantity: {
                    required: true,
                    validphone: true
                },
                price_per_unit: {
                    required: true,
                    minimumval: true
                },
                tax_amount: {
                    required: true,
                },
                total_price: {
                    required: true,
                },
                added_on: {
                    required: true,
                },
            },
            messages: {
                add_item: {
                    required: "Please enter item name."
                },
                quantity: {
                    required: "Please enter quantity."
                },
                price_per_unit: {
                    required: "Please enter price per unit."
                },
                tax_amount: {
                    required: "Tax Amount is required."
                },
                total_price: {
                    required: "Please enter total price."
                },
                added_on: {
                    required: "Please enter expense adding date."
                },
            }
        });
    });

    jQuery.validator.addMethod("validphone", function (value, element) {
        return this.optional(element) || /^[0-9]+$/.test(value);
    }, "Only  Integer Numbers are allowed");


    jQuery.validator.addMethod("minimumval", function (value, element) {
        var price_per_unit = $('#price_per_unit').val();
        if (price_per_unit <= 0) {
            $('#total_price').val(0);
            $('#tax_amount').val(0);
            return false;
        } else {
            return true;
        }

    }, "Please add a valid per unit price");

    $('#submitbutton').click(function () {

        if ($("#edit").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'ProjectExpenses', 'action' => 'addDetails']); ?>",
                data: $("#edit").serialize(),
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
                        $('#submitbutton').attr('disabled', 'disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
   //  getitems();
    function getitems() {
        itemTypeID = $('#item_type_id').val();
        //alert(itemTypeID);
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'getItems']); ?>?itemTypeId=" + itemTypeID,
            success: function (rsp) {
       // console.log(rsp);
                rsp = jQuery.parseJSON(rsp);  //console.log(rsp);
                $("#item_id").empty();
                $.each(rsp, function (i, j) {
                    $("#item_id").append('<option value="' + i + '">' + j + '</option>');
                });
                
            }
        });

    }
 
    openDiv();
    function openDiv() {
		
        if ($('#selectitems').is(":checked")) {
            $('#div1').show();
            $('#div2').hide();
            $('#div3').hide();
            $('#quantity').val(<?php echo $projectExpenses->quantity ?>);
            $('#price_per_unit').val(<?php echo $projectExpenses->price_per_unit ?>);
            sum();
        } else if ($('#additems').is(":checked"))
        {
            $('#div2').show();
            $('#div3').show();
            $('#div1').hide();
            $('#quantity').val(1);
            $('#price_per_unit').val(0);
            sum();
        }
    }
    
    function sum() {

        if ($('#selectitems').is(':checked')) {

            var item = $('#item_id').val();
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'getDetails']); ?>/" + item,
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html("Please add valid details");
                    } else {
                        tax = data.tax_value;
                        var a = $('input[name=quantity]').val();
                        var b = $('input[name=price_per_unit]').val();
                        if (b > 0) {
                           var tax_amount = ((a * b) * tax) / 100;
                            var tax_round = Math.round(tax_amount * 100) / 100
                            $('#tax_amount').val(tax_round);
                            var total_price = ((a * b) + tax_amount);
                            var result = Math.round(total_price * 100) / 100
                            $('#total_price').val(result);
                            var perunitPrice = $('#price_per_unit').val();
                            var perunitPrice = Math.round(perunitPrice * 100) / 100
                            $('#price_per_unit').val(perunitPrice);
                        } else {
                            $('#total_price').val(0);
                            $('#tax_amount').val(0);
                        }
                    }
                }
            });

        } else {
            tax = $('#tax_id').val();
            var a = $('input[name=quantity]').val();
            var b = $('input[name=price_per_unit]').val();
            if (b > 0) {
              var tax_amount = ((a * b) * tax) / 100;
                var tax_round = Math.round(tax_amount * 100) / 100
                $('#tax_amount').val(tax_round);
                var total_price = ((a * b) + tax_amount);
                var result = Math.round(total_price * 100) / 100
                $('#total_price').val(result);
                var perunitPrice = $('#price_per_unit').val();
                var perunitPrice = Math.round(perunitPrice * 100) / 100
                $('#price_per_unit').val(perunitPrice);
            } else {
                $('#total_price').val(0);
                $('#tax_amount').val(0);
            }

        }

    }
</script>
