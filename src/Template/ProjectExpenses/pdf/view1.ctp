
<?php //debug($invoicedata);?>

<style>

    .container1{
        width: 992px;
        // min-height:1403px;
        font-size: 19px;
        text-align: justify;
        line-height: 30px;     
    } 
    table, th, td {
    border-bottom: 1px solid black;
    border-collapse: collapse;
	}
	th, td {
		padding: 15px;
	}
    
</style>

<div class="container1">
		<div class="row">         
			<h1 style="text-align: center;"><?php echo $invoicedata->project_name ?></h2>
		</div>
		<div class="row">
			<table style="width: 100%;">
			<tr>
			<th style="text-align: left;"><h3 style="margin-bottom:0;">INVOICE</h3></th>
			<th><h3 style="float:right;">Date:<?php echo date('d-m-Y',strtotime($invoicetable->added_on)); ?></h3> </th>
			</tr>
			</table>
		</div>      
 
		<div class="row" style="width:100%;float:left;margin-bottom:6%;">
			<div style="width:36%;float:left;"> 
				<h3>Project Details</h3> 
					<b>Project Name:</b> <?php echo $invoicedata->project_name ?> <br>
					<b>Project Address:</b> <?php echo $invoicedata->project_address  ?><br> 
			</div>

			<div style="width:36%;float:right;">
				<h3>Client Details</h3>
				<b> Client Name:</b><?php echo $invoicedata->clientname ?><br>
				<b>Client  Address:</b> <?php echo $invoicedata->client_address ?><br> 
				<b>
			</div>
		</div>
		<div class="row">
			<table style="width: 100%;">
			<tr>
			<th style="text-align: left;"><h3 style="margin-bottom:0;">Project Expenses</h3></th>
			
			
			</tr>
			
		</div>   
		<table style="width:100%;">
		  <tr>
			<th style="text-align:left;">Item Name</th>
		     <th style="text-align:right;">Quantity</th>  
			<th style="text-align:right;">Price Per Unit</th>  
			<th style="text-align:right;">Tax Percentage</th> 
			<th style="text-align:right;">Tax Amount</th> 
			<th style="text-align:right;">Total Amount</th> 
		  </tr>
		  <tr style="background:#F1F4F7;">
			<td><h4><?php echo $invoicedata->item_name ?></h4></td>
		
			<td style="text-align:right;"> <?php echo $invoicedata->quantity ?>  </td>
			<td style="text-align:right;"> <?php echo $invoicedata->price_per_unit ?></td> 
			<td style="text-align:right;"><?php echo $invoicedata->tax_percentage?> </td>
			<td style="text-align:right;"><?php echo $invoicedata->tax_amount ?></td>
			<td style="text-align:right;"> <?php echo $invoicedata->total_price ?></td>
		  </tr>       
		</table>
		<div class="row"> 
			<div class="col-md-12"> 
					
					<p style="text-align:justify;">  
					<h4 style="padding-top:20px;"><?php echo __('Expenses Details') ?> </h4>    
					This is project expense of project <?php echo $invoicedata->project_name ?> .
					This Project expense is added by <?php echo $invoicedata->username ?>  on date <?php echo date('d-m-Y',strtotime($invoicetable->added_on)); ?> . </p>
		      
					</p>
			</div> 
		</div>
		


</div>





