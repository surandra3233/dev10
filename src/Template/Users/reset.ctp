
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Accounts Project | User Login 1</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
        <meta content="" name="author" />


        <?php
        echo $this->Html->css([ '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
            'font-awesome.min.css',
            'simple-line-icons.min.css',
            'bootstrap.min.css',
            'plugins.min.css',
            'morris.css',
            'layout.min.css',
            'darkblue.min.css',
            'custom.min.css',
            'login.min.css'
        ]);
        ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
    </head>
    <!-- END HEAD -->
    <?php echo $this->Html->script('jquery.min.js'); ?>
    <body class=" login">
		<div class="siteLogo">
           <img src="/../images/netgen-logo-large.png">
         </div>

        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php echo $this->Form->create('', array('class' => 'login-form','id' => 'login')); ?>

            <h3 class="form-title font-green">Change Password</h3>
            <?php echo $this->Flash->render(); ?>
         <!--   <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Enter new password. </span>
            </div>-->
            <div id="errMsg" class="message error" style="display:none;"> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <?php
                    echo $this->Form->input('password', array('templates' => ['inputContainer' => '{{content}}'],
                        'class' => 'form-control form-control-solid placeholder-no-fix',
                        'required' => true,
                        'label' => false,
                        'type' => 'password'
                    ));
                    ?>
                    <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9"> Confirm Password</label>
                    <?php
                    echo $this->Form->input('confirm_password', array('templates' => ['inputContainer' => '{{content}}'],
                        'class' => 'form-control form-control-solid placeholder-no-fix',
                        'required' => true,
                        'label' => false,
                        'type' => 'password'
                    ));
                    ?>

                    <div class="form-actions">
                        <button type="submit" class="btn green uppercase">Submit</button>

                    </div>
 
                  

                   
                    </form>

                </div>
           <!--     <div class="copyright">  </div>-->
                <?php
                echo $this->Html->script([
                    'jquery.min.js',
                    'bootstrap.min.js',
                    'app.min.js',
                    'morris.min.js',
                    'layout.min.js',
                    'quick-nav.min.js',
                    'login.min.js',
                    'jquery.validate.min.js'
                ]);
                ?>

                <!-- End -->
                </body>


                </html>
              
