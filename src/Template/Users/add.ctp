<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
           Users
        </li>
        <li class="active">Add User</li>
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user-plus"></i>Add Users </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->

        <?php echo $this->Form->create($user, array('class' => 'form-horizontal', 'id' => 'add')); ?>



        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">First Name</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('first_name', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Last Name</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('last_name', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">UserName</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('username', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('email', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Phone</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('phone', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Role</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('role_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $roles
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Country</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('country_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'country',
                        'options' => $countries,
                        'onChange' => "getstate();"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">State</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('state_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'state',
                        'options' => '',
                        'onChange' => "getcities();"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">city</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('city_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'city',
                        'options' => ''
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Pincode</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('pincode', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'id' => 'pin',
                            //'options' => ''
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('password', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'password'
                    ));
                    ?>
                </div>
            </div>




            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>



</div>

<?php echo $this->Html->script(['jquery.validate.min.js']);?>

<script>
    jQuery('#document').ready(function ($) {

        getstates();
        $("#add").validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                username: {
                    required: true,
                },
                phone: {
                    required: true, 
                    validphone: true,
                    minlength: 10,
                    maxlength: 10
                    
                },
                email: {
                    required: true,
                    email: true
                },
                pincode: {
                    required: true,
                    validphone: true,
                    minlength: 6,
                    maxlength: 6
                },
                password: {
                    required: true,
                }
            },
            messages: {
                first_name: {required: "Please enter first name."
                },
                last_name: {required: "Please enter last name."
                },
                username: {
                    required: 'Please enter username'
                },
                phone: {
                    required: 'Please enter phone number.'
                },
                email: {
                    required: 'Please enter email address'
                },
                pincode: {
                    required: 'Please enter pincode.',
                },
                password: {
                    required: 'Please enter password.',
                },
            }
        });
    });


    function getstates() {
        countryID = $('#country').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'States', 'action' => 'index']); ?>?country=" + countryID,
            success: function (rsp) {
                //  console.log(rsp);
                rsp = jQuery.parseJSON(rsp);
                $("#state").empty();
                $.each(rsp, function (i, j) {
                    $("#state").append('<option value="' + i + '">' + j + '</option>');
                });
                getcities();
            }
        });

    }
    function getcities() {
        stateID = $('#state').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Cities', 'action' => 'index']); ?>?state=" + stateID,
            success: function (rsp) {
                $("#city").empty();
                rsp = jQuery.parseJSON(rsp);
                $.each(rsp, function (i, j) {
                    $("#city").append('<option value="' + i + '">' + j + '</option>');
                });
            }
        });
    }


    function openEditModal(id) {

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'edit']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }

    function openViewModal(id) {

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'view']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    $('#submitbutton').click(function () {

        if ($("#add").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'addDetails']); ?>",
                data: $("#add").serialize(),
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
                        $('#submitbutton').attr('disabled','disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
      jQuery.validator.addMethod("validphone", function (value, element) {
        return this.optional(element) || /^[0-9]+$/.test(value);
    }, "Only Numbers are allowed");


</script>

