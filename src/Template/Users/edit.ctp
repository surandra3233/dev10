<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
           Users
        </li>
        <li class="active">Edit User</li>
    </ol>
</div>


<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Users </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?php echo $this->Form->create($user, array('class' => 'form-horizontal', 'id' => 'edit') ,['context' => ['valitador' => 'editdetailsUsers']]); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">First Name</label>
                <div class="col-md-6">

                    <?php
                    echo $this->Form->hidden('id');
                    echo $this->Form->input('first_name', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Last Name</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('last_name', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">UserName</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('username', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('email', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Phone</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('phone', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Role</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('role_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $roles
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Country</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('country_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'country',
                        'value'=>$user->city->state->country->id,
                        'options' => $countries,
                        'onChange' => "getstate();"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">State</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('state_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'state',
                        'value'=>$user->city->state->id,
                        'options' => $state,
                        'onChange' => "getcities();"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">city</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('city_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'city',
                        'options' => $city
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Pincode</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('pincode', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'id' => 'pin',
                            //'options' => ''
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-3 col-md-4">
                    <label class="mt-checkbox">
                        <?php
                        echo $this->Form->input('change_password', array('templates' => ['inputContainer' => '{{content}}'],
                            'label' => false,
                            'class' => 'form-control',
                           
                            'type' => 'checkbox',
                            'id' => 'change_password',
                            'onClick'=>'checkPassword();'
                               
                        ));
                        ?> Change Password
                        <span></span>
                    </label>
                </div>
            </div>
            <div class="form-group" style="display:none;" id="password_div">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('new_password', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'password'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>
            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>
</div>



<?php echo $this->Html->script(['jquery.validate.min.js']);?>
<script>
    jQuery('#document').ready(function ($) {
        //   getstates();
        $("#edit").validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                username: {
                    required: true,
                },
                phone: {
                    required: true,
                   //  validphone: true,
                    minlength: 10,
                    maxlength: 10
                },
                email: {
                    required: true,
                    email: true
                },
                pincode: {
                    required: true,
                     //  validphone: true,
                    minlength: 6,
                    maxlength: 6
                },
                new_password: {
                    required: true,
                }
            },
            messages: {
                first_name: {required: "Please enter first name."
                },
                last_name: {required: "Please enter last name."
                },
                username: {
                    required: 'Please enter username'
                },
                phone: {
                    required: 'Please enter phone number.'
                },
                email: {
                    required: 'Please enter email address'
                },
                pincode: {
                    required: 'Please enter pincode.',
                },
                new_password: {
                    required: 'Please enter new password.',
                },
            }
        });
    });


    function getstates() {
        countryID = $('#country').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'States', 'action' => 'index']); ?>?country=" + countryID,
            success: function (rsp) {
                //  console.log(rsp);
                rsp = jQuery.parseJSON(rsp);
                $("#state").empty();
                $.each(rsp, function (i, j) {
                    $("#state").append('<option value="' + i + '">' + j + '</option>');
                });
                getcities();
            }
        });

    }
    function getcities() {
        stateID = $('#state').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Cities', 'action' => 'index']); ?>?state=" + stateID,
            success: function (rsp) {
                $("#city").empty();
                rsp = jQuery.parseJSON(rsp);
                $.each(rsp, function (i, j) {
                    $("#city").append('<option value="' + i + '">' + j + '</option>');
                });
            }
        });
    }

    $('#submitbutton').click(function () {

        if ($("#edit").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'editDetails']); ?>",
                data: $("#edit").serialize(),
                success: function (data) {
					//console.log(data);
                    data = jQuery.parseJSON(data);
                  //  console.log(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
   $('#submitbutton').attr('disabled','disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });

function checkPassword(){
$('#password_div').toggle();
}
  jQuery.validator.addMethod("validphone", function (value, element) {
        return this.optional(element) || /^[0-9]+$/.test(value);
    }, "Only Numbers are allowed");


</script>

