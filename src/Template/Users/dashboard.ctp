<?php
/**
 * @var \App\View\AppView $this
 */
?>
<style>
.row
{
	margin-top:20px;
}

   
</style>


<div class="row">
	      
           <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <a class="dashboard-stat dashboard-stat-v2 blue" href="/Users/index">
                 <div class="visual">
                  <i class="fa fa-comments"></i>
                  </div>
                  <div class="details">
                     <div class="number">
                       <span data-counter="counterup" data-value="<?php echo $totalUsers;?>"><?php echo $totalUsers;?></span>
                     </div>
                     <div class="desc"> TOTAL USERS </div>
                 </div>
              </a> 
          </div>
          
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <a class="dashboard-stat dashboard-stat-v2 red" href="/Projects/index">
                 <div class="visual">
                  <i class="fa fa-bar-chart-o"></i>
                  </div>
                  <div class="details">
                     <div class="number">
                       <span data-counter="counterup" data-value="<?php echo $totalProjects;?>"><?php echo $totalProjects;?></span>
                     </div>
                     <div class="desc"> TOTAL PROJECTS </div>
                 </div>
              </a> 
          </div>
          
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <a class="dashboard-stat dashboard-stat-v2 green" href="/Items/index ">
                 <div class="visual">
                  <i class="fa fa-shopping-cart"></i>
                  </div>
                  <div class="details">
                     <div class="number">
                       <span data-counter="counterup" data-value="<?php echo $totalItems;?>"><?php echo $totalItems;?></span>
                     </div>
                     <div class="desc"> TOTAL ITEMS </div>
                 </div>
              </a> 
          </div>
                   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <a class="dashboard-stat dashboard-stat-v2 purple" href="/Taxes/index ">
                 <div class="visual">
                  <i class="fa fa-shopping-cart"></i>
                  </div>
                  <div class="details">
                     <div class="number">
                       <span data-counter="counterup" data-value="<?php echo $totalTaxes;?>"><?php echo $totalTaxes;?></span>
                     </div>
                     <div class="desc"> TOTAL TAXES </div>
                 </div>
              </a> 
          </div>         
                            
 </div>
 




<!--div  to show all managers -->
 <div class="portlet  box blue" > 
        <div class="portlet-title">
            <div class="caption mil1">
                <i class="fa fa-users"></i><?php echo __('All Managers') ?> </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-advance " id="managers">
                    <thead>
                        <tr>
                            <th><?php echo __('First Name'); ?></th>
                            <th><?php echo __('Last Name'); ?></th>
                            <th><?php echo __('UserName'); ?></th>
                            <th><?php echo __('Phone'); ?></th>
                            <th><?php echo __('Email'); ?></th>
                            <th><?php echo __('Role'); ?></th>
                            <th><?php echo __('Country'); ?></th>
                            <th><?php echo __('state'); ?></th>
                            <th><?php echo __('city'); ?></th>
                            <th><?php echo __('Pincode'); ?></th>
                            <th><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='11' align='center'> <?php echo __('Loading'); ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>	
        </div>
    </div>   
     
 <div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="Projectdetails">
					
                </div>
            </div>
        </div>
    </div>
</div>  

<!--div  to show reports -->

<div class="projectDiv">
    <!--<div class="breadcrumbs">
        <ol class="breadcrumb">
            <li>
                Reports
            </li>
            <li class="active">Generate Report</li>
        </ol>
    </div>-->
    <div class="portlet box red">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart"></i>Generate Reports </div>
            <div class="tools">

            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->

            <?php echo $this->Form->create('', array('class' => 'form-horizontal', 'id' => 'edit')); ?>
            <div class="form-body">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Project</label>
                        <div class="col-md-9">
                            <?php
                            echo $this->Form->input('project_id', array('templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'type' => 'select',
                                'id' => 'project',
                                'options' => $projects,
                                'onClick' => "getProjects();"
                            ));
                            ?>
                        </div>
                    </div></div>
                    <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Report Type</label>
                        <div class="col-md-9">
                            <?php
                            echo $this->Form->input('report_type_id', array('templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'type' => 'select',
                                'id' => 'report_type',
                                'options' => $report_type
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                   <div class="col-md-2">
                   <div class="form-group">
                       <label class="col-md-3 control-label">From</label>
                       <div class="col-md-9">
                            <?php
                            echo $this->Form->input('start_date', array('templates' => ['inputContainer' => '{{content}}'],
                              'label' => false,
                               'class' => 'form-control',
                               'id' => 'datepicker1',
                               'required' => true,
                               'type' => 'text'
                           ));
                          ?>
                     </div>
                  </div>
               </div>
                <div class="col-md-2">
                   <div class="form-group">
                       <label class="col-md-3 control-label">To</label>
                       <div class="col-md-9">
                            <?php
                            echo $this->Form->input('end_date', array('templates' => ['inputContainer' => '{{content}}'],
                              'label' => false,
                               'class' => 'form-control',
                               'id' => 'datepicker2',
                               'required' => true,
                               'type' => 'text'
                           ));
                          ?>
                     </div>
                  </div>
               </div>
                <button type="button" class="btn green" id="submitbutton">Submit</button>

                <?php echo $this->Form->end(); ?>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

<div class="portlet box blue"  style="display:none;" id="chardDiv">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-bar-chart"></i>Project Report</div>
        <div class="tools">

        </div>
    </div>
    <div class="portlet-body ">
        <div id="details"></div>

    </div>
</div>
    
<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>
<?php echo $this->Html->css(['bootstrap-datepicker3.min.css']); ?>
<?php
echo $this->Html->script([
    'bootstrap-datepicker.min.js',
    'jquery.validate.min.js',
    'fusioncharts.js',
    'fusioncharts.theme.fint.js',
    'fusioncharts-jquery-plugin.js',
    'fusioncharts.charts.js',
    'html2canvas.js',
    'jquery.plugin.html2canvas.js'
]);
?>
<script>
    $('document').ready(function () {
		 getProjects();
	  createTable();	
    })
    function createTable() {
        $('#managers').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
           "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'getManagers']); ?>",
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 10,
                    "data": function (row, type, val, meta) {
						str = "<a class='label label-sm label-success' href='javascript:;' onclick=openManagerProjectModal('" + row[10] + "')>View All Projects</a>";
						
						return str;
					  }
                }
            ]
            
        });
    }
    
    function openManagerProjectModal(id) {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'managerAllProjects']); ?>/" + id,
            success: function (rsp) {
                console.log(rsp);
                $("#Projectdetails").empty();
                $("#Projectdetails").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
     function getProjects() {
        projectId = $('#project').val();
       // alert(projectId);
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'getProjectData']); ?>?projectId=" + projectId,
            //dataType:"json",
            success: function (rsp) {
				$('#datepicker1').datepicker('remove');
				$('#datepicker2').datepicker('remove'); 
				console.log(rsp);
				rsp=JSON.parse(rsp); 
				console.log(rsp.started_on);   
				   $('#datepicker1').datepicker({
						autoclose: true,
						format: 'dd-mm-yyyy', 
				        startDate:rsp.started_on,
				        endDate:rsp.completed_on
			       });
			       $('#datepicker2').datepicker({
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        startDate:rsp.started_on,
				        endDate:rsp.completed_on
         
               });
             }
        });

    }
    
    $('#submitbutton').click(function () {
        project = $('#project').val();
        reportType = $('#report_type').val();
        startDate = $('#datepicker1').val();
        //alert(startDate );
        endDate = $('#datepicker2').val();
        if (reportType == 1) {
            $.ajax({
                url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'overAllReport']); ?>?project=" + project + "&reportType=" + reportType + "&startDate="  +startDate + "&endDate="  +endDate,
                success: function (rsp) {

                    $("#details").empty();
                    $("#details").append(rsp);
                    $('#chardDiv').show();
                }
            });
        }/* else if (reportType == 2) {

            $.ajax({
                url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'weeklyReport']); ?>?project=" + project + "&reportType=" + reportType + "&startDate="  +startDate + "&endDate="  +endDate,
                success: function (rsp) {

                    $("#details").empty();
                    $("#details").append(rsp);
                    $('#chardDiv').show();
                }
            });

        }*/ else {
            $.ajax({
                url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'itemsReport']); ?>?project=" + project + "&reportType=" + reportType + "&startDate="  +startDate + "&endDate="  +endDate,
                success: function (rsp) {

                    $("#details").empty();
                    $("#details").append(rsp);
                    $('#chardDiv').show();
                }
            });
        }

    });

</script>

