<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
           Logs
        </li>
        <li class="active">View Logs</li>
   
        
       
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
             <i class="fa fa-area-chart"></i>Logs Details</div>
         
    </div>
    <div class="portlet-body form">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <?php //debug($logs);?>
                    <tbody>
                            <tr>
                                <th> Ip</th>
                                <td> <?php  echo $logs->ip ?></td>
                            </tr>

                           <tr>
                                <th>Host Name </th>
                                <td> <?php  echo $logs->hostname ?></td>
                          </tr>

                           <tr>
                                <th>Uri </th>
                                <td>  <?php  echo $logs->uri ?>  </td>
                            </tr>
                            
                           <tr>
                                <th> Refer </th>
                                <td>  <?php  echo $logs->refer ?> </td>
                            </tr>

                            <tr>
                                <th> User Agent</th>
                                  <td>  <?php  echo $logs->user_agent ?> </td>
                            </tr>

                         
                   </tbody>
            </table>
        </div>

    </div>
</div>
