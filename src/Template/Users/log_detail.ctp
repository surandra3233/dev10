<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Logs Detail
        </li>
        <li class="active">User Logs Detail</li>
    </ol>
</div>
<?php //debug($id); ?>

<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
           <i class="fa fa-bars" aria-hidden="true"></i> User Logs Detail </div>
                            
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-advance " id="projectExpenses" style="width:100%;">
                <thead>
                    <tr>
                        <th><?php echo __('Message'); ?></th>
                        <th><?php echo __('Ip'); ?></th>
                        <th><?php echo __('Date'); ?></th>
                          
                        <th><?php echo __('Actions'); ?></th> 
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan='10' align='center'> <?php echo __('Loading'); ?> </td>
                    </tr>
                </tbody>
            </table>
        </div>	
    </div>
</div>

<div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="details">

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>
<script>
    $('document').ready(function () {
		userID='<?php echo $id ?>';
		//alert(userID);
        createExpenseTable(userID);
       });
     
   function createExpenseTable(userID) {
        $('#projectExpenses').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'getLogs']); ?>/" +userID,
            
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 3,
                    "data": function (row, type, val, meta) {
						
						
					    var str = "&nbsp;<a class='label label-sm label-info' href='javascript:;' onclick=openViewLogModal('" + row[3] + "')>View</a>";
						

					
					return str;
						
                    }
                }
            ]
            
        });
    }
    function openViewLogModal(id) {
	    $('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'viewlog_detail']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
</script>
