
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Accounts Project | User Login 1</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
        <meta content="" name="author" />


        <?php
        echo $this->Html->css([ '//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
            'font-awesome.min.css',
            'simple-line-icons.min.css',
            'bootstrap.min.css',
            'plugins.min.css',
            'morris.css',
            'layout.min.css',
            'darkblue.min.css',
            'custom.min.css',
            'login.min.css'
        ]);
        ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <!-- END HEAD -->
    <?php echo $this->Html->script('jquery.min.js'); ?>
    <body class=" login">

<div class="siteLogo">
 <img src="/../images/netgen-logo-large.png">
</div>
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php echo $this->Form->create('', array('class' => 'login-form')); ?>

            <h3 class="form-title font-green">Sign In</h3>
            <?php echo $this->Flash->render(); ?>
           <!-- <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span> Enter email and password. </span>
            </div>-->
            <div id="errMsg" class="message error" style="display:none;"> </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Email</label>
                <?php
                echo $this->Form->input('email', array('templates' => ['inputContainer' => '{{content}}'],
                    'class' => 'form-control form-control-solid placeholder-no-fix',
                    'required' => true,
                    'label' => false,
                    'type' => 'text'
                ));
                ?>
               <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <?php
                    echo $this->Form->input('password', array('templates' => ['inputContainer' => '{{content}}'],
                        'class' => 'form-control form-control-solid placeholder-no-fix',
                        'required' => true,
                        'label' => false,
                        'type' => 'password'
                    ));
                    ?>
               
              <?php $display='none';
              if (isset($_SESSION['failedAttempt'])) { ?>
			     <?php if($_SESSION['failedAttempt'] >= 3) { 
				  $display='block';
			       ?>
		          <?php //debug($_SESSION['failedAttempt']);?>
                  <br><div class="g-recaptcha" data-sitekey="6LfQSS0UAAAAABsdwAe_c5C4MV71DCv8VDqxNB6d"></div>
                  <?php } ?>
        	  <?php } ?>	
        		
                    <div class="form-actions">
                        <button type="submit"  id="loginBTN" class="btn green uppercase">Login</button>
                       	
			<?php //debug($_SESSION['failedAttempt']);?>
                    

                    </div>
 
                   <br>
                      <a href='/Users/login1'>Forgot Password</a>

                   
                    </form>

                </div>
               
            
                <?php
                echo $this->Html->script([
                    'jquery.min.js',
                    'bootstrap.min.js',
                    'app.min.js',
                    'morris.min.js',
                    'layout.min.js',
                    'quick-nav.min.js',
                    'login.min.js',
                    'jquery.validate.min.js'
                ]);
                ?>

                <!-- End -->
                </body>
</html>



<script>

	$(document).ready(function() {
		$('.btn').click(function() {
			if($('#email').val()=='' && $('#password').val()=='' ) {
				$('#errMsg').html('Please enter Email and Password');
				//$('#errMsg').css('color','red');
				$('#errMsg').show();
			} else if($('#email').val()=='') {
				$('#errMsg').html('Please enter Email.');
				//$('#errMsg').css('color','red');
				$('#errMsg').show();
			} else if($('#password').val()=='') {
				$('#errMsg').html('Please enter Password.');
				//$('#errMsg').css('color','red');
				$('#errMsg').show();
			}
		});
	});
	
</script>

              

