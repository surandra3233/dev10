<?php
/**
 * @var \App\View\AppView $this
 */
?>
<?php //echo $this->Flash->render(); ?>

<style>
.row
{
	margin-top:20px;
}

</style>

 
<div class="row">
	      
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <a class="dashboard-stat dashboard-stat-v2 red" href="/Projects/client_projects">         
                 <div class="visual">
                  <i class="fa fa-bar-chart-o"></i>
                  </div>
                  <div class="details">
                     <div class="number">
                       <span data-counter="counterup" data-value="<?php echo $projectsCount;?>"><?php echo $projectsCount;?></span>
                     </div>
                     <div class="desc"> TOTAL PROJECTS </div>
                 </div>
              </a> 
          </div>
          
                
                            
 </div>
 
<!--div  to show reports -->

<div class="projectDiv">
 <!--   <div class="breadcrumbs">
        <ol class="breadcrumb">
            <li>
                Reports
            </li>
            <li class="active">Generate Report</li>
        </ol>
    </div>-->
    <div class="portlet box red">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart"></i>Generate Reports </div>
            <div class="tools">

            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->

            <?php echo $this->Form->create('', array('class' => 'form-horizontal', 'id' => 'edit')); ?>
            <div class="form-body">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Project</label>
                        <div class="col-md-9">
                            <?php
                            echo $this->Form->input('project_id', array('templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'type' => 'select',
                                'id' => 'project',
                                'empty'=>'Select Project',
                                'options' => $projects,
                                'onClick' => "getProjects();"
                            ));
                            ?>
                        </div>
                    </div></div><div class="col-md-2">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Report Type</label>
                        <div class="col-md-9">
                            <?php
                            echo $this->Form->input('report_type_id', array('templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'class' => 'form-control',
                                'required' => true,
                                'type' => 'select',
                                'id' => 'report_type',
                                'options' => $report_type
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                   <div class="form-group">
                       <label class="col-md-3 control-label">From</label>
                       <div class="col-md-9">
                            <?php
                            echo $this->Form->input('start_date', array('templates' => ['inputContainer' => '{{content}}'],
                              'label' => false,
                               'class' => 'form-control',
                               'id' => 'datepicker1',
                               'required' => true,
                               'type' => 'text'
                           ));
                          ?>
                     </div>
                  </div>
               </div>
                <div class="col-md-2">
                   <div class="form-group">
                       <label class="col-md-3 control-label">To</label>
                       <div class="col-md-9">
                            <?php
                            echo $this->Form->input('end_date', array('templates' => ['inputContainer' => '{{content}}'],
                              'label' => false,
                               'class' => 'form-control',
                               'id' => 'datepicker2',
                               'required' => true,
                               'type' => 'text'
                           ));
                          ?>
                     </div>
                  </div>
               </div>
                <button type="button" class="btn green" id="submitbutton">Submit</button>

                <?php echo $this->Form->end(); ?>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

<div class="portlet box blue"  style="display:none;" id="chardDiv">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-bar-chart"></i>Project Report</div>
        <div class="tools">

        </div>
    </div>
    <div class="portlet-body ">
        <div id="details"></div>

    </div>
</div>
  
<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>
<?php echo $this->Html->css(['bootstrap-datepicker3.min.css']); ?>
<?php
echo $this->Html->script([
    'bootstrap-datepicker.min.js',
    'jquery.validate.min.js',
    'fusioncharts.js',
    'fusioncharts.theme.fint.js',
    'fusioncharts-jquery-plugin.js',
    'fusioncharts.charts.js',
    'html2canvas.js',
    'jquery.plugin.html2canvas.js'
]);
?>  

<script>
	 $('document').ready(function () {
	  getProjects();
	  $("#edit").validate({
            rules: {
                start_date: {
                    required: true,
                  
                },
                end_date: {
                    required: true,
                },
              
                
            },
            messages: {
                start_date: {
                    required: "Please select starting date range."
                },
                end_date: {
                    required: "Please select ending date range."
                },
               
            }
        });
        
    }); 
    
     function getProjects() {
        projectId = $('#project').val();
       // alert(projectId);
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'getProjectData']); ?>?projectId=" + projectId,
            //dataType:"json",
            success: function (rsp) {
				$('#datepicker1').datepicker('remove');
				$('#datepicker2').datepicker('remove'); 
				console.log(rsp);
				rsp=JSON.parse(rsp); 
				console.log(rsp.started_on);   
				   $('#datepicker1').datepicker({
						autoclose: true,
						format: 'dd-mm-yyyy', 
				        startDate:rsp.started_on,
				        endDate:rsp.completed_on
			       });
			       $('#datepicker2').datepicker({
                        autoclose: true,
                        format: 'dd-mm-yyyy',
                        startDate:rsp.started_on,
				        endDate:rsp.completed_on
         
               });
             }
        });

    }
	$('#submitbutton').click(function () {
        project = $('#project').val();
        reportType = $('#report_type').val();
        startDate = $('#datepicker1').val();
        //alert(startDate );
        endDate = $('#datepicker2').val();
         //alert(endDate );
        if (reportType == 1) {
            $.ajax({
                url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'overAllReport']); ?>?project=" + project + "&reportType=" + reportType + "&startDate="  +startDate + "&endDate="  +endDate,
                success: function (rsp) {

                    $("#details").empty();
                    $("#details").append(rsp);
                    $('#chardDiv').show();
                }
            });
        } /*else if (reportType == 2) {

            $.ajax({
                url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'weeklyReport']); ?>?project=" + project + "&reportType=" + reportType + "&startDate="  +startDate + "&endDate="  +endDate,
                success: function (rsp) {

                    $("#details").empty();
                    $("#details").append(rsp);
                    $('#chardDiv').show();
                }
            });

        }*/ else {
            $.ajax({
                url: "<?php echo $this->Url->build(['controller' => 'Reports', 'action' => 'itemsReport']); ?>?project=" + project + "&reportType=" + reportType + "&startDate="  +startDate + "&endDate="  +endDate,
                success: function (rsp) {

                    $("#details").empty();
                    $("#details").append(rsp);
                    $('#chardDiv').show();
                }
            });
        }

    });

</script>
