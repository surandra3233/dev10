<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
           Users
        </li>
        <li class="active">View User</li>
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>User Details</div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <?php // debug($user);?>
                <tbody>
                    <tr>
                        <th> First Name </th>
                        <td> <?php echo $user->first_name ?> </td>

                    </tr>
                    <tr>
                        <th> Last Name </th>
                        <td> <?php echo $user->last_name ?> </td>

                    </tr>
                    <tr>
                        <th> UserName </th>
                        <td> <?php echo $user->username ?> </td>

                    </tr>
                    <tr>
                        <th> Role </th>
                        <td> <?php echo $user->role->role_name ?> </td>

                    </tr>
                    <tr>
                        <th> Email </th>
                        <td> <?php echo $user->email ?> </td>

                    </tr>
                    <tr>
                        <th> Phone </th>
                        <td> <?php echo $user->phone ?> </td>

                    </tr>
                    <tr>
                        <th> country </th>
                        <td> <?php echo $user->city->state->country->country_name ?> </td>

                    </tr> <tr>
                        <th> State </th>
                        <td> <?php echo $user->city->state->state_name ?> </td>

                    </tr>

                    <tr>
                        <th> City </th>
                        <td> <?php echo $user->city->city_name ?> </td>

                    </tr>

                    <tr>
                        <th> Pin Code </th>
                        <td> <?php echo $user->pincode ?> </td>

                    </tr>


                </tbody>
            </table>
        </div>

    </div>
</div>
