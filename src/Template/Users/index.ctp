<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <!--<h1>All Users</h1>-->
    <ol class="breadcrumb">
        <li>
            Users
        </li>
        <li class="active">All Users</li>
    </ol>
</div>

<div class="col-md-12">
    <?php echo $this->Flash->render(); ?>

    <div class="portlet portlet box red" > 
        <div class="portlet-title">
            <div class="caption mil1">
                <i class="fa fa-users"></i><?php echo __('All Users') ?> </div>

            <div class="actions">

                <a href="javascript:;" class="btn blue-madison" onClick="addUser();">
                    <i class="fa fa-plus"></i> Add New User</a>
            </div>



        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-advance " id="users">
                    <thead>
                        <tr>

                            <th><?php echo __('First Name'); ?></th>
                            <th><?php echo __('Last Name'); ?></th>
                            <th><?php echo __('UserName'); ?></th>
                            <th><?php echo __('Phone'); ?></th>
                            <th><?php echo __('Email'); ?></th>
                            <th><?php echo __('Role'); ?></th>
                            <th><?php echo __('Country'); ?></th>
                            <th><?php echo __('state'); ?></th>
                            <th><?php echo __('city'); ?></th>
                            <th><?php echo __('Pincode'); ?></th>
                            <th><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='11' align='center'> <?php echo __('Loading'); ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>	
        </div>
    </div>    
</div>
<div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="details">

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>

<script>
    jQuery('#document').ready(function ($) {
        allusers();

    });
    function allusers() {

        $('#users').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 10,
                    "data": function (row, type, val, meta) {
                        str = "<a class='label label-sm label-success' href='javascript:;' onclick=openViewModal('" + row[10] + "')>View</a>"
                        str += "&nbsp;<a class='label label-sm label-danger' href='javascript:;' onclick=openEditModal(" + row[10] + ")>Edit</a>";
                        str += "&nbsp;<a class='label label-sm label-info' href='javascript:;' onclick=openLogModal(" + row[10] + ")>Logs</a>";
                        return str;


                    }
                }
            ],
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'index1']); ?>",
        });
    }

    function openEditModal(id) {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'edit']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }

    function openViewModal(id) {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'view']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    
    
    function openLogModal(id) {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'logDetail']); ?>/" + id, 
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }    

    function addUser() {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'add']); ?>",
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });

    }
</script>

