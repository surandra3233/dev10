<style>
    .error{
        color:red;
    }
    .form-horizontal .form-group {
        margin-left: 0px;
        margin-right: 0px;
    }
    @media all and (max-width: 768px){
        input#is-deactivated {
            margin-top: 2px;
        }
    }
</style>



<div class="breadcrumbs">
<!--    <h1>My Profile</h1>-->
    <ol class="breadcrumb">
        <li>
           Users
        </li>
        <li class="active">User Profile</li>
    </ol>
</div>


 
<div class="row">
    <div class="col-md-4">

        <div class="portlet light profile-sidebar-portlet bordered">

            <div class="profile-usertitle centerprofile">
                <div class="profile-usertitle-name"> <h4><?php echo $user->first_name . " " . $user->last_name; ?></h4>
                <button type="button" class="btn btn-circle red btn-sm"><?php echo $user->role->role_name; ?></button>
                
                </div>
                <br>
                <table class="table table-hover" >
                    
                    <tr>
                    
                        <td class='alert alert-info centerprofile'>   Username :  <?php echo $user->username; ?></td>
                    </tr>
                    <tr>
                        <td class='alert alert-danger centerprofile'>Email : <?php echo $user->email; ?></td>
                    </tr>
                    <tr>
                        <td class='alert alert-success centerprofile'>Phone :  <?php echo $user->phone; ?> </td>
                    </tr>
                    
                 



                </table>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="row" style="margin-bottom:5px;">
            <?= $this->Flash->render() ?>
        </div>
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">User Profile</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a aria-expanded="true" href="#tab_1_1" data-toggle="tab">Personal Info</a>
                    </li>

                    <li class="">
                        <a aria-expanded="false" href="#tab_1_3" data-toggle="tab">Change Password</a>
                    </li>


                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <!-- PERSONAL INFO TAB -->
                    <div class="tab-pane active" id="tab_1_1">
                        <?php echo $this->Form->create($user, array('id' => 'edit', 'class' => 'form-horizontal', 'type' => 'file', 'novalidate' => true), ['context' => ['valitador' => 'editdetailsUsers']]) ?>
                        <div class="form-body">
                            <div class="portlet-body form">


                                <div class="form-group">
                                    <label class="col-md-3 control-label">First Name</label>
                                    <div class="col-md-6">

                                        <?php
                                        echo $this->Form->hidden('id');
                                        echo $this->Form->input('first_name', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'text'
                                        ));
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Last Name</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('last_name', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'text'
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">UserName</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('username', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'text'
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('email', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'text'
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Phone</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('phone', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'text'
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Role</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('role_id', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'select',
                                            'options' => $roles
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Country</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('country_id', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'select',
                                            'id' => 'country',
                                            'value'=>$user->city->state->country->id,
                                            'options' => $countries,
                                            'onChange' => "getstate();"
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">State</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('state_id', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'select',
                                            'id' => 'state',
                                            'value'=>$user->city->state->id,
                                            'options' => $state,
                                            'onChange' => "getcities();"
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">city</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('city_id', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'select',
                                            'id' => 'city',
                                            'options' => $city
                                        ));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Pincode</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('pincode', array('templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'class' => 'form-control',
                                            'required' => true,
                                            'type' => 'text',
                                            'id' => 'pin',
                                              
                                        ));
                                        ?>
                                    </div>
                                </div>


                                <div class="form-actions">
                                    <button type="submit" class="btn green" >Submit</button>
                                </div>
                                <?php echo $this->Form->end(); ?>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>

                    <!--Change Password Tab-->
                    <div class="tab-pane" id="tab_1_3">
                        <?php echo $this->Form->create($user, array('id' => 'change_password_form', 'url' => ['action' => 'passwordedit'], 'context' => ['validator' => 'changePassword'])); ?>
                        <?php echo $this->Form->hidden('id', array('value' => $user->id)); ?>
                        <div class="form-group">
                            <label class="control-label">Current Password *</label>
                            <?php
                            echo $this->Form->input('cpassword', array('div' => false, 'label' => false, 'placeholder' => 'Current Password', 'class' => 'form-control', 'value' => '', 'id' => 'cpassword', 'error' => false, 'type' => 'password'));
                            if ($this->Form->isFieldError('cpassword')) {
                                echo $this->Form->error('cpassword');
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">New Password *</label>
                            <?php
                            echo $this->Form->input('npassword', array('div' => false, 'label' => false, 'placeholder' => 'New Password', 'class' => 'form-control', 'value' => '', 'id' => 'npassword', 'error' => false, 'type' => 'password'));
                            if ($this->Form->isFieldError('npassword')) {
                                echo $this->Form->error('npassword');
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Re-type New Password *</label>
                            <?php
                            echo $this->Form->input('passwordconfirm', array('div' => false, 'label' => false, 'placeholder' => 'Confirm Password', 'class' => 'form-control', 'value' => '', 'id' => 'confirmpassword', 'error' => false, 'type' => 'password'));
                            if ($this->Form->isFieldError('passwordconfirm')) {
                                echo $this->Form->error('passwordconfirm');
                            }
                            ?>
                        </div>  
                        <div class="form-actions">
                            <div class="row" style="margin-left:0px">
                                <div class="margin-top-10" style="margin-bottom:10px;">
                                    <?php echo $this->Form->submit('Submit', array('class' => 'btn green')); ?>
                                    <!--                                    <button type="submit" class="btn green">Submit</button>-->
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script(['jquery.validate.min.js']);?>
<script>
    $(document).ready(function () {
        $("#edit").validate({
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                username: {
                    required: true,
                },
                phone: {
                    required: true,
                    validphone: true,
                    minlength: 10,
                    maxlength: 10

                },
                email: {
                    required: true,
                    email: true
                },
                pincode: {
                    required: true,
                     validphone: true,
                    minlength: 6,
                    maxlength: 6
                },
                new_password: {
                    required: true,
                }
            },
            messages: {
                first_name: {required: "Please enter first name."
                },
                last_name: {required: "Please enter last name."
                },
                username: {
                    required: 'Please enter username'
                },
                phone: {
                    required: 'Please enter phone number.'
                },
                email: {
                    required: 'Please enter email address'
                },
                pincode: {
                    required: 'Please enter pincode.',
                },
                new_password: {
                    required: 'Please enter new password.',
                },
            }
        });



        $("#change_password_form").validate({
            rules: {
                cpassword: {
                    required: true,
                   // minlength: 8,
                },
                npassword: {
                    required: true,
                   // minlength: 8,
                },
                passwordconfirm: {
                    required: true,
                    passMatch: true
                },
            },
            messages: {
                cpassword: {
                    required: "Please enter your current password",
//                    pasformat: "Please add atleast 1 Uppercase letter, 1 lower case letter, 1 number and 1 special character.",
                   //minlength: "Minimum 5 characters required"
                },
                npassword: {required: "Please enter your new password",
//                    pasformat: "Please add atleast 1 Uppercase letter, 1 lower case letter, 1 number and 1 special character.",
                  // minlength: "Minimum 5 characters required"
                },
                passwordconfirm: {
                    required: 'Please re enter your password',
                    passMatch: 'Your passwords do not match. Please re enter your password',
                }
            }
        });
    });
    function getstates() {
        countryID = $('#country').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'States', 'action' => 'index']); ?>?country=" + countryID,
            success: function (rsp) {
                //  console.log(rsp);
                rsp = jQuery.parseJSON(rsp);
                $("#state").empty();
                $.each(rsp, function (i, j) {
                    $("#state").append('<option value="' + i + '">' + j + '</option>');
                });
                getcities();
            }
        });

    }
    function getcities() {
        stateID = $('#state').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Cities', 'action' => 'index']); ?>?state=" + stateID,
            success: function (rsp) {
                $("#city").empty();
                rsp = jQuery.parseJSON(rsp);
                $.each(rsp, function (i, j) {
                    $("#city").append('<option value="' + i + '">' + j + '</option>');
                });
            }
        });
    }
//    jQuery.validator.addMethod("pasformat", function (value, element) {
//        return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/.test(value);
//    }, "Please add atleast 1 Uppercase letter, 1 lower case letter, 1 number and 1 special character.");

    jQuery.validator.addMethod('passMatch', function (value, element) {
        var newpass = $("#npassword").val();
        var confirmpass = $("#confirmpassword").val();
        if (newpass != confirmpass) {
            return false;
        } else {
            return true;
        }

    });

    jQuery.validator.addMethod("validphone", function (value, element) {
        return this.optional(element) || /^[0-9]+$/.test(value);
    }, "Only Numbers are allowed");




</script>

