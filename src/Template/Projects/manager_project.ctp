<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
           Projects
        </li>
        <li class="active">All Projects</li>
    </ol>
</div>

   <div class="col-md-12">
        <?php echo $this->Flash->render(); ?>

        <div class="portlet portlet box red" > 
            <div class="portlet-title">
                <div class="caption mil1">
                    <i class="fa fa-area-chart"></i><?php echo __('All Projects') ?> </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-advance " id="projects">
                        <thead>
                            <tr>

                                <th><?php echo __('Project Name'); ?></th>
                                <th><?php echo __('Client'); ?></th>
                                <th><?php echo __('Country'); ?></th>
                                <th><?php echo __('state'); ?></th>
                                <th><?php echo __('city'); ?></th>
                                <th><?php echo __('Pincode'); ?></th>
                                 <th><?php echo __('Started on'); ?></th>
                                <th><?php echo __('Completion Date'); ?></th>
                                <th><?php echo __('Estimated Budget'); ?></th>
                                <th><?php echo __('Approved Budget'); ?></th>
                                 <th><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='10' align='center'> <?php echo __('Loading'); ?> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>	
            </div>
        </div>    
    </div>
       
  <div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
           <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="details">

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>
<script>
    $('document').ready(function () {
        createTable();
        
    });
    function createTable() {
        $('#projects').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
           "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'getManagerProjects']); ?>",
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 10,
                    "data": function (row, type, val, meta) {
						str = "<a class='label label-sm label-success' href='javascript:;' onclick=openViewModal('" + row[10] + "')>View</a>";
						
					//	str += "&nbsp;<a class='label label-sm label-info' href='javascript:;' onclick=openEditModal('" + row[10] + "')>Edit</a>";
						
                        str += "&nbsp;<a class='label label-sm label-danger' href='javascript:;' onclick=openViewExpensesModal('" + row[10] + "')>Project Expenses</a>";
					
					   
						return str;
						
						
                       
                    }
                }
            ]
            
        });
    }
      function openViewModal(id) {
	    $('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'view']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
     function openEditModal(id) {
		$('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'edit']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    function openAddExpensesModal(id) {
		$('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();
    
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'ProjectExpenses', 'action' => 'add']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
     function openViewExpensesModal(id) {
		$('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'ProjectExpenses', 'action' => 'view']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
 
   
</script>
