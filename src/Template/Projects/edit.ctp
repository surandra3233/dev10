<?php
/**
 * @var \App\View\AppView $this
 */
?>
<style>

    #managerselect{
        width: 100%; 
        overflow: hidden; 
        text-overflow: ellipsis;
    }</style>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Projects
        </li>
        <li class="active">Edit Projects</li>
    </ol>
</div>

<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-area-chart"></i>Edit Project </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?php echo $this->Form->create($project, array('class' => 'form-horizontal', 'id' => 'edit')); ?>
        <?php //debug($project);die;?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">Project Name</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->hidden('id');
                    echo $this->Form->input('project_name', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Manager</label>
                <div class="col-md-8"  >

                    <?php
                    echo $this->Form->input('manager', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control multiselect dropdown-toggle mt-multiselect btn btn-default',
                        'required' => true,
                        'type' => 'select',
                        'options' => $manager,
                        'id' => 'managerselect',
                        'value' => $selectedarr,
                        'multiple' => "multiple"
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Client</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('user_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $client
                    ));
                    ?>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-md-3 control-label">Country</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('country_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'country',
                        'value' => $project->city->state->country->id,
                        'options' => $countries,
                        'onChange' => "getstate();"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">State</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('state_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'state',
                        'value' => $project->city->state->id,
                        'options' => $state,
                        'onChange' => "getcities();"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">city</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('city_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'id' => 'city',
                        'options' => $city,
                        'onChange' => "getpincode();"
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Pincode</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('pincode', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'id' => 'pin'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Started On</label>
                <div class="col-md-8">

                    <?php
                    echo $this->Form->input('started_on', array('started_on' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'id' => 'datepicker1',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>

                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Completion Date</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('completed_on', array('started_on' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'id' => 'datepicker2',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Estimated Budget</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('estimated_budget', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'onblur' => 'roundvalue();',
                        'id' => 'budgetEstimate'
                    ));
                    ?>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Approved Budget</label>
                <div class="col-md-8">
                    <?php
                    echo $this->Form->input('approved_budget', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'onblur' => 'roundvalue();',
                        'id' => 'budgetApproved'
                    ));
                    ?>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>
            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>
</div>

<?php echo $this->Html->script(['jquery.validate.min.js', 'bootstrap-datepicker.min.js', 'bootstrap-multiselect.js']); ?>
<?php echo $this->Html->css(['bootstrap-datepicker3.min.css', 'bootstrap-multiselect.css']); ?>



<script>
    $('document').ready(function ()
    {
         $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
          }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                 $('#datepicker2').datepicker('setStartDate', startDate);
                 }).on('clearDate', function (selected) {
                      $('#datepicker2').datepicker('setStartDate', null);
                    });
         $('#datepicker2').datepicker({
             format: 'dd-mm-yyyy',
             autoclose: true,
           }).on('changeDate', function (selected) {
                 var endDate = new Date(selected.date.valueOf());
                 $('#datepicker1').datepicker('setEndDate', endDate);
                 }).on('clearDate', function (selected) {
                      $('#datepicker1').datepicker('setEndDate', null);
                    });
                    
        $('#managerselect').multiselect();
        
        $("#edit").validate({
            rules: {
                project_name: {
                    required: true,
                    minlength: 2,
                },
                manager: {
                    required: true,
                },
                pincode: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    onlynumber: true,
                },
                started_on: {
                    required: true,
                },
                completed_on: {
                    required: true,
                },
                estimated_budget: {
                    required: true,
                    validphone: true,
                },
                approved_budget: {
                    required: true,
                    validphone: true,
                },
            },
            messages: {
                project_name: {
                    required: "Please enter project name."
                },
                manager: {
                    required: "Please select one."
                },
                pincode: {
                    required: "Please enter pincode."
                },
                started_on: {
                    required: "Please enter starting date."
                },
                completed_on: {
                    required: "Please enter completion date."
                },
                estimated_budget: {
                    required: "Please enter estimated budget."
                },
                approved_budget: {
                    required: "Please enter approved budget."
                },
            }
        });
    });

    function getstates() {
        countryID = $('#country').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'States', 'action' => 'index']); ?>?country=" + countryID,
            success: function (rsp) {
                //  console.log(rsp);
                rsp = jQuery.parseJSON(rsp);
                $("#state").empty();
                $.each(rsp, function (i, j) {
                    $("#state").append('<option value="' + i + '">' + j + '</option>');
                });
                getcities();
            }
        });

    }
    function getcities() {
        stateID = $('#state').val();
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Cities', 'action' => 'index']); ?>?state=" + stateID,
            success: function (rsp) {
                $("#city").empty();
                rsp = jQuery.parseJSON(rsp);
                $.each(rsp, function (i, j) {
                    $("#city").append('<option value="' + i + '">' + j + '</option>');
                });
            }
        });
    }


    $('#submitbutton').click(function () {

        if ($("#edit").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'saveDetails']); ?>",
                data: $("#edit").serialize(),
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
                        $('#submitbutton').attr('disabled', 'disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
    
    jQuery.validator.addMethod("onlynumber", function (value, element) {
        return this.optional(element) || /^[0-9]+$/.test(value);
    }, "Only Numbers are allowed");

    jQuery.validator.addMethod("validphone", function (value, element) {
        return this.optional(element) || /^[0-9.]+$/.test(value);
    }, "Only Integer and Decimal Numbers are allowed");


    function roundvalue() {
        budgetEstimate = $('#budgetEstimate').val();
        budgetApproved = $('#budgetApproved').val();
        if (budgetEstimate != '') {
            $('#budgetEstimate').val(Math.round(budgetEstimate * 100) / 100);
        }
        if (budgetApproved != '') {
            $('#budgetApproved').val(Math.round(budgetApproved * 100) / 100);
        }
    }
</script>

