
  <?php// debug($id);?> 
        
        <div class="portlet portlet box red" > 
            <div class="portlet-title">
                <div class="caption mil1">
                    <i class="fa fa-area-chart"></i><?php echo __('All Projects') ?> </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-advance " id="projects">
                        <thead>
                            <tr>

                                <th><?php echo __('Project Name'); ?></th>
                                <th><?php echo __('Client'); ?></th>
                                <th><?php echo __('Country'); ?></th>
                                <th><?php echo __('state'); ?></th>
                                <th><?php echo __('city'); ?></th>
                                <th><?php echo __('Pincode'); ?></th>
                                 <th><?php echo __('Started on'); ?></th>
                                <th><?php echo __('Completion Date'); ?></th>
                                <th><?php echo __('Estimated Budget'); ?></th>
                                <th><?php echo __('Approved Budget'); ?></th>
                                 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='9' align='center'> <?php echo __('Loading'); ?> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>	
            </div>
        </div>    
   
       
 

<script>
    $('document').ready(function () {
		userId='<?php echo $id ?>';
		//alert(userId);
		
        createTable();
        
    });
    function createTable() {
        $('#projects').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
           "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Projects', 'action' => 'getManagerAllProjects']); ?>/"+userId,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                   
                }
            ]
            
        });
    }
      
     
   
     
 
   
</script>
