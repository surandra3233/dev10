<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
           Projects
        </li>
        <li class="active">View Projects</li>
    </ol>
</div>
<?php //debug($project->id);  ?>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
             <i class="fa fa-area-chart"></i>Projects Details</div>
         <?php if($this->request->session()->read('Auth.User.role_id')==1){?>  
        <div class="actions"> <a href="javascript:;" class="btn blue-madison" onClick="viewBudget(<?php echo $project->id;  ?>);">
                  View Budget Details</a> </div>
           <?php } else { ?><div class="actions"> <a href="javascript:;" class="btn blue-madison" onClick="viewProjectBudget(<?php echo $project->id;  ?>);">
                  View Budget Details</a> </div> <?php } ?>

    </div>
    <div class="portlet-body form">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <?php //debug($project);?>
                    <tbody>
                            <tr>
                                <th> Project Name</th>
                                <td> <?php echo $project->project_name ?></td>
                            </tr>

                           <tr>
                                <th> Manager </th>
                                <td> <?php foreach($project->project_users as $user):?>
                                         <?= $user->user->first_name." ".$user->user->last_name."," ?>  
                                         <?php endforeach;?>
                                </td>
                            </tr>

                           <tr>
                                <th> Client </th>
                                <td> <?php echo $project->client->first_name." ".$project->client->last_name ?>  </td>
                            </tr>
                            
                           <tr>
                                <th> Country </th>
                                <td> <?php echo $project->city->state->country->country_name ?>  </td>
                            </tr>

                            <tr>
                                <th> State</th>
                                  <td> <?php echo $project->city->state->state_name ?>  </td>
                            </tr>

                            <tr>
                                <th> city </th>
                               <td> <?php echo $project->city->city_name ?>  </td>
                            </tr>

                            <tr>
                                <th> pincode</th>
                                <td> <?php echo $project->pincode ?> </td>
                            </tr>
                            <tr>
                                <th> Started On</th>
                                <td><?php echo $project->started_on ?>  </td>
                            </tr>
                            <tr>
                                <th> Completion Date</th>
                                <td><?php echo $project->completed_on ?>  </td>
                            </tr>
                            <tr>
                                <th> Estimated Budget</th>
                               <td><?php echo $project->estimated_budget ?>  </td>
                            </tr>

                            <tr>
                                <th> Approved Budget</th>
                                <td><?php echo $project->approved_budget ?>  </td>
                            </tr>
                            <tr>
                                <th> Total Payment Recieved</th>
                                <td><?php echo $paymentsum ?>  </td>
                            </tr>
                   </tbody>
            </table>
        </div>

    </div>
</div>

 <div class="portlet box blue" style="display:none;" id="budgetbox"> 
            <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-area-chart "></i>Projects Budget Details</div>
                <div class="actions"> <a href="javascript:;" class="btn yellow-crusta" onClick="openBudgetModal(<?php echo $project->id;  ?>);">
                   Assign Budget</a> </div>
             </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-advance " id="budgetdetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <th><?php echo __('Amount'); ?></th>
                                <th><?php echo __('Alloted By'); ?></th>
                                <th><?php echo __('Alloted To'); ?></th>
                                <th><?php echo __('Alloted On'); ?></th>      
                                <th><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='5' align='center'> <?php echo __('Loading'); ?> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>	
            </div>
   </div>  


<div class="portlet box blue" style="display:none;" id="managerbudgetbox">
            <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-user"></i>Projects Budget Details</div>
                
             </div>
            <div class="portlet-body">
                <div class="table-responsive">
                     <table class="table table-striped table-bordered table-advance " id="Managerbudgetdetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <th><?php echo __('Amount'); ?></th>
                                <th><?php echo __('Alloted By'); ?></th>
                                <th><?php echo __('Alloted To'); ?></th>
                                <th><?php echo __('Alloted On'); ?></th>      
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='5' align='center'> <?php echo __('Loading'); ?> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>	
            </div>
   </div>  


<script>
    function viewBudget(id) {
		
         $('#budgetdetails').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'ProjectBudgets', 'action' => 'index1']); ?>/" + id,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 4,
                    "data": function (row, type, val, meta) {
						
						str1 = "&nbsp;<a class='label label-sm label-info' href='javascript:;' onclick=openBudgetEditModal('" + row[4] + "')>edit</a>";
//						str1 += "&nbsp;<a class='label label-sm label-danger' href='javascript:;' onclick=openDeleteModal('" + row[3] + "')>Delete</a>";
						str1 += "&nbsp;<a class='label label-sm label-success'  href='/ProjectBudgets/invoice.pdf?BudgetID=" + row[4] + "'>Generate Budget Invoice</a>"
						return str1;
			
                    }
                }
            ]
            
        });
        $('#budgetbox').toggle();
        }
        
         function viewProjectBudget(id) {

         $('#Managerbudgetdetails').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'ProjectBudgets', 'action' => 'index1']); ?>/" + id,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    
                }
            ]
            
        });
        $('#managerbudgetbox').toggle();
        }
        
        function openBudgetEditModal(id) {
			projectId='<?php echo $project->id;  ?>';
		//alert(projectId);
	    $('#SuccessDiv1').hide();
        $('#ErrorDiv1').hide();

        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'ProjectBudgets', 'action' => 'edit']); ?>/" + id+'/'+projectId,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
         function openDeleteModal(id) {
	     $('#SuccessDiv1').hide();
         $('#ErrorDiv1').hide();

         $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'ProjectBudgets', 'action' => 'delete']); ?>/"+id,
               
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {

                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
    }
    
    </script>
