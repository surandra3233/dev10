<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Items
        </li>
        <li class="active">Edit Item</li>
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list" aria-hidden="true"></i>
            Edit Item </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
   <?php// debug($id); ?>
        <?php echo $this->Form->create($item, array('class' => 'form-horizontal', 'id' => 'edit')); ?>


        <div class="form-body">
		    <div class="form-group">
                <label class="col-md-3 control-label">Item Type</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('item_type_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'id'=>'item_type_id',
                        'type' => 'select',
                        'options' => $itemTypes,
                        'onChange' => "getitems();"
                    ));
                    ?>
                </div>
            </div>
            
       
            <div class="form-group">
                <label class="col-md-3 control-label">Item Name</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->hidden('id');
                    echo $this->Form->input('item_name', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'id'=>'item_name',
                        'type' => 'select',
                        'options' => $items,
                        'value' => $item->id
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Tax Percentage</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('tax_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $taxes
                    ));
                    ?>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>



</div>

<?php echo $this->Html->script(['jquery.validate.min.js']); ?>

<script>
    jQuery('#document').ready(function ($) {


        $("#edit").validate({
            rules: {
                item_name: {
                    required: true,
                },
                tax_id: {
                    required: true,
                }
            },
            messages: {
                item_name: {required: "Please enter the item name."
                },
                tax_id: {required: "Please select the tax percentage."
                },
            }
        });
    });



    $('#submitbutton').click(function () {

        if ($("#edit").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'editDetails']); ?>",
                data: $("#edit").serialize(),
                success: function (data) {
				   data = jQuery.parseJSON(data);
                   if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
            $('#submitbutton').attr('disabled','disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
   // getitems();
    function getitems() {
        itemTypeID = $('#item_type_id').val();
        //alert(itemTypeID);
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'getItems']); ?>?itemTypeId=" + itemTypeID,
            success: function (rsp) {
       // console.log(rsp);
                rsp = jQuery.parseJSON(rsp);  //console.log(rsp);
                $("#item_name").empty();
                $.each(rsp, function (i, j) {
                    $("#item_name").append('<option value="' + i + '">' + j + '</option>');
                });
                
            }
        });

    }

</script>

