<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <!--<h1>All Users</h1>-->
    <ol class="breadcrumb">
        <li>
            Items
        </li>
        <li class="active">All Items</li>
    </ol>
</div>

<div class="col-md-12">
    <?php echo $this->Flash->render(); ?>

    <div class="portlet portlet box red" > 
        <div class="portlet-title">
            <div class="caption mil1">
                <i class="fa fa-list" aria-hidden="true"></i>
                <?php echo __('Items') ?> </div>
            <div class="actions">
                <a href="javascript:;" class="btn blue-madison" onClick="addItem();">
                    <i class="fa fa-plus"></i> Add Items</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-advance " id="items">
                    <thead>
                        <tr>
                            <th><?php echo __('Item Type '); ?></th>
                            <th><?php echo __('Item Name'); ?></th>
                            <th><?php echo __('Tax Percentage'); ?></th>
                            <th><?php echo __('Added By'); ?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Action'); ?></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='6' align='center'> <?php echo __('Loading'); ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>	
        </div>
    </div>    
</div>
<div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="details">

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>

<script>
    jQuery('#document').ready(function ($) {  
        allitems();

    });
    function allitems() {
        $('#items').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 4,
                    "data": function (row, type, val, meta) {
                        if (row[4] == 1) {
                            str = "<label class='label label-sm label-warning'  )>Archived</a>";
                        } else {
                            str = "<label class='label label-sm label-info'  )>Active</a>";
                        }

                        return str;
                       }
                },
                {
                    "targets": 5,
                    "data": function (row, type, val, meta) {
                        
                        str = "&nbsp;&nbsp;<a class='label label-sm label-info' href='javascript:;' onclick=openEditModal('" + row[5] + "')>Edit</a>";
                        return str;


                    }
                }
                
            ],
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'allItems']); ?>",
        });
    }

   function openEditModal(id) {
    $('#SuccessDiv1').hide();
    $('#ErrorDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'edit']); ?>/" + id, 
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    function addItem() {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'add']); ?>",
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });

    }

</script>

