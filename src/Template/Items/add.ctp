<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Items
        </li>
        <li class="active">Add Item</li>
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-list" aria-hidden="true"></i>
            Add Item </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <?php //debug($itemTypes);die;?>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->

        <?php echo $this->Form->create($item, array('class' => 'form-horizontal', 'id' => 'add')); ?>

       <div class="form-body">
		    <div class="form-group">
                <label class="col-md-3 control-label">Item Type</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('item_type_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $itemTypes,
                        'id'=>'item_type_id',
                        
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Item Name</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('item_name', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text',
                        'id'=>'item_name'
                        
                    ));
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Tax Percentage</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('tax_id', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'select',
                        'options' => $taxes
                    ));
                    ?>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>



</div>

<?php echo $this->Html->script(['jquery.validate.min.js']); ?>

<script>
    jQuery('#document').ready(function ($) {


        $("#add").validate({
            rules: {
                item_name: {
                    required: true,
                },
                tax_id: {
                    required: true,
                }
            },
            messages: {
                item_name: {required: "Please enter the item name."
                },
                tax_id: {required: "Please select the tax percentage."
                },
            }
        });
    });



    $('#submitbutton').click(function () {

        if ($("#add").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Items', 'action' => 'saveDetails']); ?>",
                data: $("#add").serialize(),
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
 $('#submitbutton').attr('disabled','disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
   
  
</script>

