<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="breadcrumbs">
    <!--<h1>All Users</h1>-->
    <ol class="breadcrumb">
        <li>
            Taxes
        </li>
        <li class="active">All Taxes</li>
    </ol>
</div>

<div class="col-md-12">
    <?php echo $this->Flash->render(); ?>

    <div class="portlet portlet box red" > 
        <div class="portlet-title">
            <div class="caption mil1">
                <i class="fa fa-indent" aria-hidden="true"></i><?php echo __('Taxes') ?> </div>

            <div class="actions">

                <a href="javascript:;" class="btn blue-madison" onClick="addTax();">
                    <i class="fa fa-plus"></i> Add Tax</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-advance " id="taxes">
                    <thead>
                        <tr>

                            <th><?php echo __('Tax Amount'); ?></th>
                            <th><?php echo __('Description'); ?></th>
                            <th><?php echo __('Status'); ?></th>
                            <th><?php echo __('Actions'); ?></th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='4' align='center'> <?php echo __('Loading'); ?> </td>
                        </tr>
                    </tbody>
                </table>
            </div>	
        </div>
    </div>    
</div>
<div id="model" class="modal fade  bs-modal-lg" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">               
                <div id="ErrorDiv1" class="message error" style="display:none;"><span id="ErrorMsg1"></span></div>
                <div id="SuccessDiv1" class="message success" style="display:none;"><span id="SuccessMsg1"></span></div>
                <div id="details">

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>

<script>
    jQuery('#document').ready(function ($) {
        alltaxes();

    });
    function alltaxes() {
        $('#taxes').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                    "targets": 2,
                    "data": function (row, type, val, meta) {
                        if (row[2] == 1) {
                            str = "<label class='label label-sm label-warning'  )>Archived</a>";
                        } else {
                            str = "<label class='label label-sm label-info'  )>Active</a>";
                        }

                        return str;


                    }
                },
                {
                    "targets": 3,
                    "data": function (row, type, val, meta) {
                        if (row[2] == 0) {
                            str = "<a class='label label-sm label-danger' href='<?php echo $this->Url->build(['controller' => 'Taxes', 'action' => 'archive']); ?>/" + row[3] + "' )>Archive</a>";
                        } else {
                            str = "<a class='label label-sm label-success' href='<?php echo $this->Url->build(['controller' => 'Taxes', 'action' => 'archive']); ?>/" + row[3] + "' )>Activate</a>";
                        }

                        return str;


                    }
                }
            ],
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Taxes', 'action' => 'index1']); ?>",
        });
    }


    function addTax() {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Taxes', 'action' => 'add']); ?>",
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });

    }
    function Archivetax(id) {
        $('#ErrorDiv1').hide();
        $('#SuccessDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Taxes', 'action' => 'archive']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });

    }
</script>

