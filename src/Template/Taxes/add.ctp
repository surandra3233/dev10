<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Taxes
        </li>
        <li class="active">Add Tax</li>
    </ol>
</div>
<div class="portlet box red">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-indent" aria-hidden="true"></i>Add Tax </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
            <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
            <a href="javascript:;" class="remove" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <?php echo $this->Form->create($tax, array('class' => 'form-horizontal', 'id' => 'add')); ?>
        <div class="form-body">
            <div class="form-group">
                <label class="col-md-3 control-label">Tax Value</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('tax_value', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Description</label>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->input('description', array('templates' => ['inputContainer' => '{{content}}'],
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true,
                        'type' => 'text'
                    ));
                    ?>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" class="btn green" id="submitbutton">Submit</button>

            </div>
            <?php echo $this->Form->end(); ?>
            <!-- END FORM-->
        </div>
    </div>
</div>
<?php echo $this->Html->script(['jquery.validate.min.js']); ?>
<script>
    jQuery('document').ready(function ($) {

        $("#add").validate({
            rules: {
                tax_value: {
                    required: true,
                    validvalue: true
                },
                description: {
                    required: true,
                }
            },
            messages: {
                first_name: {required: "Please enter the tax value."
                },
                description: {required: "Please enter the description."
                },
            }
        });
    });

    $('#submitbutton').click(function () {

        if ($("#add").valid() == true) {
            $.ajax({
                type: "POST",
                url: "<?php echo $this->Url->build(['controller' => 'Taxes', 'action' => 'saveDetails']); ?>",
                data: $("#add").serialize(),
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.success == false) {
                        $('#ErrorDiv1').show();
                        $('#ErrorMsg1').html(data.message);

                    } else {
                        $('#submitbutton').attr('disabled', 'disabled');
                        $('#SuccessDiv1').show();
                        $('#SuccessMsg1').html(data.message);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                }
            });
        }
    });
    
    jQuery.validator.addMethod("validvalue", function (value, element) {
        return this.optional(element) || /^[0-9.]+$/.test(value);
    }, "Only Integer and Decimal Numbers are allowed");
</script>

