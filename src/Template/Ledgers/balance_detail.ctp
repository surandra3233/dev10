<?php
/**
 * @var \App\View\AppView $this
 */
?>
<?php //debug($sum);?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Ledger Balance
        </li>
        <li class="active">Projects Balance </li>
    </ol>
</div>
<div class="portlet box blue"  id="balncebox">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-area-chart"></i>Projects Balance Details </div>
       
       
    </div>
    <div class="portlet-body form" >
      <div class="table-responsive">
                    <table class="table table-striped table-bordered table-advance " id="balancedetails" style="width: 100%;">
                        <tbody>
                            <tr>
                                <th> Total Project Budget</th>
                                <td><?php echo $budgetsum ?></td>
                            </tr>

                            <tr>
                                <th> Total Project Expense </th>
                                <td><?php echo $expensesum ?>	  </td>
                             </tr>
                            
                            <tr>
                                <th> Your Balance </th>
                                <td> <?php echo $balance ?>	 </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>	

    </div>
</div>
