<?php
/**
 * @var \App\View\AppView $this
 */
?>
<?php //debug($sum);?>
<div class="breadcrumbs">
    <ol class="breadcrumb">
        <li>
            Ledger
        </li>
        <li class="active">Project Ledger</li>
    </ol>
</div>


<div class="portlet box red">
    <div class="portlet-title">
       <div class="caption mil1">
                    <i class="fa fa-bars"></i><?php echo __('Ledger') ?> </div>
                 <div class="actions">

                <a href="javascript:;" class="btn blue-madison" onClick="viewBalance(<?php echo $id ;?>);">
                    <i class="fa fa-plus"></i>Ledger Balance</a>
            </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-advance " id="ledgers" style="width:100%;">
                <thead>
                    <tr>
                        <th><?php echo __('Transaction Type'); ?></th>
                        <th><?php echo __('Amount'); ?></th>
                        <th><?php echo __('Particulars'); ?></th>
                        <th><?php echo __('Transaction date'); ?></th>
                
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan='10' align='center'> <?php echo __('Loading'); ?> </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>



 
<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>
<script>
    $('document').ready(function () {
      
        ledgerID='<?php echo $id ?>';
          createTable(ledgerID);
    });
    function createTable(ledgerID) {
        $('#ledgers').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "bDestroy": true,
            "sAjaxSource": "<?php echo $this->Url->build(['controller' => 'Ledgers', 'action' => 'index1']); ?>/" + ledgerID,
            "columnDefs": [{"className": "dt-center", "targets": "_all"},
                {
                }
            ]
            
        });
    }
    
   function viewBalance(id) {
      $('#SuccessDiv1').hide();
      $('#ErrorDiv1').hide();
        $.blockUI({
            message: '<h3>Loading</h3>',
            css: {
                border: 'none',
                padding: '8px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: "<?php echo $this->Url->build(['controller' => 'Ledgers', 'action' => 'balanceDetail']); ?>/" + id,
            success: function (rsp) {
                $("#details").empty();
                $("#details").append(rsp);
                $('#model').modal('show');
                $.unblockUI();
            }
        });
    }
    
  </script>  
     
