<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ledgers Controller
 *
 * @property \App\Model\Table\LedgersTable $Ledgers
 */
class LedgersController extends AppController
{
 public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
    }

    public function isAuthorized($user = null) {
        $adminArr = array( 'getledger', 'index1','balanceDetail');
        $mangerArr = array();
        $clientArr = array('getledger', 'index1','balanceDetail');
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }
    
    
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function getledger($id)
    {   
		
	  $this->set( 'id',$id);
	
       
    }
    
    
    public function index1($id)
    {
        $aColumns = array('transaction_types.transaction_type_name',
            'ledgers.amount',
            'ledgers.particulars',
            'ledgers.transaction_date',
            'ledgers.id',
        );
        $sIndexColumn = " ledgers.id ";
        $sTable = " ledgers ";
        $sJoinTable = ' INNER JOIN transaction_types transaction_types ON transaction_types.id=ledgers.transaction_type_id';
        $sConditions = 'ledgers.project_id =' . $id;
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
   
        $j=0;
        foreach($returnArr['aaData'] as $return)
        {
			$returnArr['aaData'][$j]['transaction_date']=date('d-m-Y',strtotime($return['transaction_date']));
			$returnArr['aaData'][$j][3]=date('d-m-Y',strtotime($return['transaction_date']));
			$j++;
		}
		
        echo json_encode($returnArr);
        die;
    }
    /*balanceDetail
     * 
     */
    public function balanceDetail($id)
    {
	
		$this->loadModel('ProjectBudgets');
        $projectBudget = $this->ProjectBudgets
                ->find('all')
                ->where(['project_id =' => $id])->toArray();
        $budgetsum=0; $j=0;
        foreach ($projectBudget as $key => $data) {
		  $budgetsum=$budgetsum+$data['amount'];
	      $j++;
	    }

	    $this->loadModel('ProjectExpenses');
        $projectExpense = $this->ProjectExpenses
                ->find('all')
                ->where(['project_id =' => $id]);
        $expensesum=0; $j=0;
        foreach ($projectExpense as $key => $data) {
	      $expensesum=$expensesum+$data['total_price'];
	      $j++;
	    }
	    $balance=$budgetsum-$expensesum;
	    $this->set(compact( 'id','expensesum','budgetsum','balance', $id,$expensesum,$budgetsum,$balance));
	}
  
}
