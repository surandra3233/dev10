<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;
use Cake\I18n\Time;

/**
 * Payments Controller
 *
 * @property \App\Model\Table\PaymentsTable $Payments
 */
class PaymentsController extends AppController
{
	
	 public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
        $this->loadComponent('Captcha.Captcha');
    }
	
	public function isAuthorized($user = null) {
        $adminArr = array('add', 'addDetails', 'index', 'index1', 'edit', 'editDetails' , 'view','getPayments' ,'paymentDetail');
        $mangerArr = array();
        $clientArr = array();
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }

    /**
     * Index method
     *
     */
    public function index()
    {   
		$this->viewBuilder()->layout('admin_layout');
		$projects = $this->Payments->Projects->find('list', ['limit' => 200]);
       
        $this->set(compact('projects'));
    
    }
    
   /**
     * Index1 method
     * Create datatble displaying all payments
     */
    public function index1() {
		$project = $this->request->query['projectId'];
	    $aColumns = array('users.username',
            'projects.project_name',
            'payments.amount',
            'payments.date',
            'payments.refrence_no',
            'payments.id',
        );
        $sIndexColumn = " payments.id ";
        $sTable = " payments ";
        $sJoinTable = 'INNER JOIN projects projects ON projects.id=payments.project_id INNER JOIN users users ON users.id=payments.user_id ' ;
    
         if (empty($project)) {
                   $sConditions = '';
               } else {
                  $sConditions = 'payments.project_id=' . $project;
               }
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        $j=0;
        foreach($returnArr['aaData'] as $r) {
			$returnArr['aaData'][$j]['date']=date('d-m-Y',strtotime($r['date']));
			$returnArr['aaData'][$j][3]=date('d-m-Y',strtotime($r['date']));
			
			$j++;
		} 
        echo json_encode($returnArr);
        die;
    }
    
    
    /**
     * View method
     *
     */
    public function view($id)
    {
		
		$this->loadModel('ProjectExpenses');
        $payment = $this->Payments
                ->find('all')
                ->where(['project_id =' => $id]);
        $this->set(compact('payment', 'id', $payment, $id));
        $this->set('_serialize', ['payment']);
		
     }
     
     
   /**
     * Add method
     * add new payment 
     */
    public function add() {
        $payment = $this->Payments->newEntity();
        
        $projects = $this->Payments->Projects
                                      ->find('list', ['limit' => 200]);
        $clients = $this->Payments->Users
                                      ->find('list')
                                      ->where(['role_id =' => 3]);
        $this->set(compact('payment', 'projects', 'clients'));
        $this->set('_serialize', ['payment']);
    }
    
     /**
     * AddDetails method 
     * save payment after adding payment details
     */
    public function addDetails() {
		 $payment = $this->Payments->newEntity();
		 $dateForMysql = date('Y-m-d', strtotime($this->request->data['date']));
         $this->request->data['date'] = $dateForMysql;
         $payment = $this->Payments->patchEntity($payment, $this->request->data, ['validate' => 'Add']);
         $errors = [];
         if (!$payment->errors()) {
			 $result=$this->Payments->save($payment);
		
             if ($result) {
			     $arr['success'] = true;
                 $arr['message'] = 'The payment has been saved.';
             } else {
                 $arr['success'] = false;
                 $arr['message'] = 'The payment could not be saved. Please, try again.';
               }
		 } else {
           $errors = implode('<br>-> ', array_values(Hash::flatten($payment->errors())));
           $errors = '-> ' . $errors;
           $arr['success'] = false;
           $arr['message'] = $errors;
         }
        echo json_encode($arr);
        die;
	   
    }

     /**
     * Edit method
     * edit payment 
     *
     */
    
    public function edit($id)
    {
		//debug($id);die;
        $payment = $this->Payments->get($id, [
            'contain' => ['Users','Projects']
        ]);
        $payment->date=$payment->date->format('d-m-Y');
        $projects = $this->Payments->Projects
                                      ->find('list', ['limit' => 200]);
        $clients = $this->Payments->Users
                                      ->find('list')
                                      ->where(['role_id =' => 3]);
        $this->set(compact('payment', 'projects', 'clients'));
        $this->set('_serialize', ['payment']);
    }
    
    /**
     * editDetails method 
     * save payment after editing payment details
     */
  
    public function editDetails()
     {
       $payment = $this->Payments->get($this->request->data['id']);
         if ($this->request->is(['patch', 'post', 'put'])) {
           $dateForMysql = date('Y-m-d', strtotime($this->request->data['date']));
           $this->request->data['date'] = $dateForMysql;
           $payment = $this->Payments->patchEntity($payment, $this->request->data);
        }
		 
        
         $errors = [];
         if (!$payment->errors()) {
			 $result=$this->Payments->save($payment);
             if ($result) {
			    $arr['success'] = true;
                 $arr['message'] = 'The payment has been saved.';
             } else {
                 $arr['success'] = false;
                 $arr['message'] = 'The payment could not be saved. Please, try again.';
               }
		 } else { 
           $errors = implode('<br>-> ', array_values(Hash::flatten($payment->errors())));
           $errors = '-> ' . $errors;
           $arr['success'] = false;
           $arr['message'] = $errors;
         }
        echo json_encode($arr);
        die;
	   
    }
    
    /**
     * Delete method
     *
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $payment = $this->Payments->get($id);
        if ($this->Payments->delete($payment)) {
            $this->Flash->success(__('The payment has been deleted.'));
        } else {
            $this->Flash->error(__('The payment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
