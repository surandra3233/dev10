<?php

namespace App\Controller;

use Cake\I18n\Time;
use App\Controller\AppController;
use Cake\Log\Log;


/**
 * ProjectBudgets Controller
 *
 * @property \App\Model\Table\ProjectBudgetsTable $ProjectBudgets
 */
class ProjectBudgetsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
    }

    public function isAuthorized($user = null) {
        $adminArr = array('add', 'saveDetails', 'index1', 'edit', 'delete', 'createInvoice' ,'invoice');
        $mangerArr = array('index1');
        $clientArr = array('index1');
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index1($id) {
		
        $aColumns = array('project_budgets.amount',
            'users.username',
            'user.username',              
            'project_budgets.alloted_on',
            'project_budgets.id',
        );
        $sIndexColumn = " project_budgets.id ";
        $sTable = " project_budgets ";
        $sJoinTable = ' INNER JOIN users users ON users.id=project_budgets.user_id  
                        INNER JOIN users user ON user.id=project_budgets.manager_id 
                        ';  
        $sConditions = 'project_budgets.project_id =' . $id;
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        $j=0;
       foreach($returnArr['aaData'] as $return)
       {
		   $returnArr['aaData'][$j]['alloted_on']=date('d-m-Y',strtotime($return['alloted_on']));
		   $returnArr['aaData'][$j][3]=date('d-m-Y',strtotime($return['alloted_on']));
		   $j++;
	   }
	   echo json_encode($returnArr);
        die;
    }

    /**
     * View method
     *
     * @param string|null $id Project Budget id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $projectBudget = $this->ProjectBudgets->get($id, [
            'contain' => ['Projects', 'Users']
        ]);
        $this->set('projectBudget', $projectBudget);
        $this->set('_serialize', ['projectBudget']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id) {
        $projectBudget = $this->ProjectBudgets->newEntity();
        $projects = $this->ProjectBudgets->Projects->find('list', ['limit' => 200]);
        $users = $this->ProjectBudgets->Users->find('list', ['conditions' => ['role_id' => 1]]);
        $this->loadModel('Projects');
        $projectdata=$this->Projects->find('all')
                                ->where(['id'=>$id]);
         $this->loadModel('ProjectUsers');
        $projectuser=$this->ProjectUsers->find('all')
                                ->where(['project_id'=>$id]);
        $newUser=array();
        foreach($projectuser as $user)
        {
			$newUser[]=$user['user_id'];
			
		}   
		$this->loadModel('Users');
        $manager=$this->Users->find('list')
                                ->select(['username']) 
                                ->where(['id IN'=>$newUser])->toArray();     
        $this->set(compact('projectBudget', 'projects', 'users', 'id','projectdata','manager'));
        $this->set('_serialize', ['projectBudget']);
    }
    /**
     * saveDetails method
     *
     * 
     */

    public function saveDetails() { 
		$this->loadModel('Projects');
		$project = $this->Projects
                              ->find('all')
                              ->select(['project_name'])
                              ->where(['id =' => $this->request->data['project_id']])
                              ->toArray() ;
        foreach($project as $project1)
        {
		   $project1->project_name;
	    }
	
        $this->loadModel('Ledgers');
        $allotedOn = new \DateTime($this->request->data['alloted_on']);
        $this->request->data['alloted_on'] = $allotedOn;
        if (isset($this->request->data['id'])) {
            $projectBudget = $this->ProjectBudgets->get($this->request->data['id']);
            $ledger = $this->Ledgers->get($projectBudget->ledger_id);
        } else {
            $ledger = $this->Ledgers->newEntity();
            $projectBudget = $this->ProjectBudgets->newEntity();
        }
        $ledgerDetails['project_id'] = $this->request->data['project_id'];
        $ledgerDetails['transaction_type_id'] = 2;
        $ledgerDetails['particulars'] = $this->request->data['amount'] . " alloted to project " . $project1->project_name;
        $ledgerDetails['amount'] = $this->request->data['amount'];
        $ledgerDetails['transaction_date'] = $this->request->data['alloted_on'];
        $ledger = $this->Ledgers->patchEntity($ledger, $ledgerDetails);
        $ledgerresult = $this->Ledgers->save($ledger);
        if ($ledgerresult) {
            $this->request->data['ledger_id'] = $ledgerresult->id;
            $projectBudget = $this->ProjectBudgets->patchEntity($projectBudget, $this->request->data);
            $errors = [];
            if (!$projectBudget->errors()) {
                $budgetDetails = $this->ProjectBudgets->save($projectBudget);
                $this->loadModel('Users');
		        $user = $this->Users
                              ->find('all')
                              ->select(['username'])
                              ->where(['id =' => $budgetDetails->user_id])
                              ->toArray() ;
               foreach($user as $user1)
               {
		          $user1->username;
	            }
               
                if ($budgetDetails) {
                    $invoice = $this->createInvoice($budgetDetails->id);
                    if ($invoice) {
						if (isset($this->request->data['id'])) {
                           Log::write('info', 'Project Budget of project ' ." ".$project1->project_name." ".'edited sucessfully and alloted by user'." ". $user1->username,['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                        } else {
                            Log::write('info', 'Project Budget of project ' ." ".$project1->project_name." ".'added sucessfully and alloted by user'." ". $user1->username,['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                           }
                        $arr['success'] = true;
                        $arr['message'] = 'The project budget has been saved.';
                    } else {
                         if (!isset($this->request->data['id'])) {
                           
                            $this->ProjectBudgets->delete($budgetDetails);
                            $this->ProjectBudgets->delete($ledgerresult);
                            
                         } 
                        $arr['success'] = false;
                        $arr['message'] = 'The project budget could not be saved. Please, try again.';
                    }
                } else {
                    $arr['success'] = false;
                    $arr['message'] = 'The project budget could not be saved. Please, try again.';
                }
            } else {
                $errors = implode('<br>-> ', array_values(Hash::flatten($user->errors())));
                $errors = '-> ' . $errors;
                $arr['success'] = false;
                $arr['message'] = $errors;
            }
        } else {
            $arr['success'] = false;
            $arr['message'] = 'The project budget could not be saved. Please, try again.';
        }
        echo json_encode($arr);
        die;
    }

    /**
     * Edit method
     *
     * 
     */
    public function edit($id,$projectId) {
	    $projectBudget = $this->ProjectBudgets->get($id);
        $projectBudget->alloted_on=$projectBudget->alloted_on->format('d-m-Y'); 
        $manager=$this->ProjectBudgets->find('all')
                                       
                                       ->where(['project_id'=>$projectId])
                                       ->toArray();
        $newUser=array();
        foreach($manager as $user)
        {
			$newUser[]=$user['manager_id'];
			
		}
		 $this->loadModel('Users');                           
         $managerList=$this->Users->find('list')
                                ->select(['username']) 
                                ->where(['id IN'=>$newUser])->toArray(); 
        $this->loadModel('Projects');
        $projectdata=$this->Projects->find('all')
                                ->where(['id'=>$projectId])
                                ->toArray();
                          
        $users = $this->ProjectBudgets->Users->find('list', ['conditions' => ['role_id' => 1]]);
        $managers = $this->ProjectBudgets->Users->find('list', ['conditions' => ['role_id' => 2]]);
        $this->set(compact('projectBudget', 'users', 'id' , 'managerList' , 'projectdata'));
        $this->set('_serialize', ['projectBudget']);
    }

    /**
     * Delete method
     *
     * 
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $projectBudget = $this->ProjectBudgets->get($id);
        $this->loadModel('Ledgers');
        $ledger = $this->Ledgers->get($projectBudget->ledger_id);
        if ($this->Ledgers->delete($ledger)) {
            if ($this->ProjectBudgets->delete($projectBudget)) {
                $arr['success'] = true;
                $arr['message'] = 'The project budget has been deleted.';
            } else {
                $ledger = $this->Ledgers->newEntity();
                $ledgerDetails['project_id'] = $projectBudget->project_id;
                $ledgerDetails['transaction_type_id'] = 2;
                $ledgerDetails['particulars'] = $projectBudget->amount . " alloted to project " . $projectBudget->project_id;
                $ledgerDetails['amount'] = $projectBudget->amount;
                $ledgerDetails['transaction_date'] = $projectBudget->alloted_on;
                $ledger = $this->Ledgers->patchEntity($ledger, $ledgerDetails);
                $ledgerresult = $this->Ledgers->save($ledger);
                $arr['message'] = 'The project budget could not be deleted. Please, try again.';
            }
        } else {
            $arr['success'] = false;
            $arr['message'] = 'The project budget could not be deleted. Please, try again.';
        }
        echo json_encode($arr);
        die;
    }

    public function createInvoice($budgetID) {
		$projectBudget=$this->ProjectBudgets->find()->where(['id'=>$budgetID])->first();
		$this->loadModel('Projects');
		$project=$this->Projects->find()->where(['id'=>$projectBudget->project_id])->first();
		$this->loadModel('Users');
		$client=$this->Users->find()->where(['id'=>$project->user_id])->first();
	    $this->loadModel('Cities');
		$city=$this->Cities->find()->where(['id'=>$client->city_id])->first();
		$this->loadModel('States');
		$state=$this->States->find()->where(['id'=>$city->state_id])->first();
        $this->loadModel('Countries');
		$country=$this->Countries->find()->where(['id'=>$state->country_id])->first();
        $this->loadModel('Invoices');
        $invoicedetail = $this->Invoices->find()->where(['project_budget_id' => $budgetID])->first();
        if (!Empty($invoicedetail)) {
            $invoice = $this->Invoices->get($invoicedetail->id);
        } else {
            $invoice = $this->Invoices->newEntity();
        }
        $projectBudget = $this->ProjectBudgets->find()->where(['ProjectBudgets.id' => $budgetID])->Contain(['Projects','Users', 'Projects.Cities', 'Projects.Cities.States', 'Projects.Cities.States.Countries'])->first();
        $user=$this->ProjectBudgets->Users->find()->select(['username'])->where(['id'=>$projectBudget->user_id])->toArray();
        $manager=$this->ProjectBudgets->Users->find()->select(['username'])->where(['id'=>$projectBudget->manager_id])->toArray();
        foreach($user as $admin)
        {    
			//debug($admin->username);
		}
		 $manager=$this->ProjectBudgets->Users->find()->select(['username'])->where(['id'=>$projectBudget->manager_id])->toArray();
		  foreach($manager as $managerData)
        {  
			  //debug($managerData->username);
		}
        $invoiceArr['project_budget_id'] = $projectBudget->id;
        $invoiceArr['content']['alloted_by'] = $admin->username;
        $invoiceArr['content']['alloted_to'] = $managerData->username;
        $invoiceArr['content']['clientname']=$client->username;
		$invoiceArr['content']['client_address']=$city->city_name .' ' .$state->state_name . ' ' .$country->country_name;
	    $invoiceArr['content']['project_name'] = $projectBudget->project->project_name;
        $invoiceArr['content']['project_address'] = $projectBudget->project->city->city_name . ' ' . $projectBudget->project->city->state->state_name . ' ' . $projectBudget->project->city->state->country->country_name;
        $invoiceArr['content']['amount'] = $projectBudget->amount;
        $invoiceArr['added_on'] = $projectBudget->alloted_on;
        $arr = json_encode($invoiceArr['content']);
        $invoiceArr['content'] = $arr;
        $invoiceArr=$this->Invoices->patchEntity($invoice,$invoiceArr);
        $result=$this->Invoices->save($invoiceArr);
       if($result){
          return true; 
       }else{
          return false; 
    }
                
  }
    public function invoice()
        {  
         $id=$this->request->query['BudgetID'];
		 $this->loadModel('Invoices');
		 $invoicetable=$this->Invoices->find()->where(['project_budget_id'=>$id])->first();
		 $invoiceId=$invoicetable->id;
		 $invoicedata=json_decode($invoicetable->content);
		   $this->viewBuilder()->options([
              'pdfConfig' => [
                   'orientation' => 'portrait',
                   'filename' => 'Invoice_' . $invoiceId.'.pdf',
                   'download' => (bool)$this->request->query('download')
               ]
           ]);
      
         $this->set(compact('invoicedata', 'invoicetable', $invoicedata, $invoicetable));
         $this->set('_serialize', ['invoicetable']);
         }

}
