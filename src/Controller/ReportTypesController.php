<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ReportTypes Controller
 *
 * @property \App\Model\Table\ReportTypesTable $ReportTypes
 */
class ReportTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $reportTypes = $this->paginate($this->ReportTypes);

        $this->set(compact('reportTypes'));
        $this->set('_serialize', ['reportTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Report Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reportType = $this->ReportTypes->get($id, [
            'contain' => ['Reports']
        ]);

        $this->set('reportType', $reportType);
        $this->set('_serialize', ['reportType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reportType = $this->ReportTypes->newEntity();
        if ($this->request->is('post')) {
            $reportType = $this->ReportTypes->patchEntity($reportType, $this->request->data);
            if ($this->ReportTypes->save($reportType)) {
                $this->Flash->success(__('The report type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The report type could not be saved. Please, try again.'));
        }
        $this->set(compact('reportType'));
        $this->set('_serialize', ['reportType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Report Type id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reportType = $this->ReportTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reportType = $this->ReportTypes->patchEntity($reportType, $this->request->data);
            if ($this->ReportTypes->save($reportType)) {
                $this->Flash->success(__('The report type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The report type could not be saved. Please, try again.'));
        }
        $this->set(compact('reportType'));
        $this->set('_serialize', ['reportType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Report Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reportType = $this->ReportTypes->get($id);
        if ($this->ReportTypes->delete($reportType)) {
            $this->Flash->success(__('The report type has been deleted.'));
        } else {
            $this->Flash->error(__('The report type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
