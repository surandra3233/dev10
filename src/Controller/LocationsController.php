<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Locations Controller
 *
 * @property \App\Model\Table\LocationsTable $Locations
 */
class LocationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $city = $this->request->query['city'];
       // debug($city);die;
        $locations = $this->Locations->find('list')->where(['city_id' => $city]);
        echo json_encode($locations);
        die;
    }
}