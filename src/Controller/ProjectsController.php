<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\I18n\Date;
use Cake\Log\Log;
use  App\Controller\AuthComponent;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class ProjectsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
    }

    public function isAuthorized($user = null) {
        $adminArr = array('add', 'index', 'edit', 'view', 'saveDetails', 'addDetails', 'getProjects' , 'projectdetails' ,'managerAllProjects','getManagerAllProjects');  
        $mangerArr = array('managerProject', 'view', 'edit', 'saveDetails', 'getManagerProjects');
        $clientArr = array('clientProjects', 'getClientProjects', 'view');
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
	   $this->viewBuilder()->layout('admin_layout');
    }

    /**
     * Add method
     *
     */
    public function add() {
        $project = $this->Projects->newEntity();

        $this->loadModel('Users');
        $manager = $this->Users
                ->find('list')
                ->where(['role_id =' => 2]);

        $client = $this->Users
                ->find('list')
                ->where(['role_id =' => 3]);

        $this->loadModel('Countries');
        $countries = $this->Countries->find('list', ['limit' => 200]);

        $this->set(compact('project', 'countries', 'manager', 'client', 'projectUsers', 'managerDetails'));
        $this->set('_serialize', ['project', 'projectUsers', 'managerDetails']);
    }

    /**
     * Edit method
     *
     */
    public function edit($id) {
	    $project = $this->Projects->get($id, [
            'contain' => ['Cities', 'ProjectUsers.Users', 'Cities.States', 'Cities.States.Countries']
        ]);
        $project->started_on=$project->started_on->format('d-m-Y');
        $project->completed_on=$project->completed_on->format('d-m-Y'); 
      
        foreach ($project->project_users as $key => $data) {
            $selectedarr[$data->user_id] = $data->user_id;
        }
        $this->loadModel('Users');
        $manager = $this->Users
                ->find('list')
                ->where(['role_id =' => 2]);

        $client = $this->Users
                ->find('list')
                ->where(['role_id =' => 3]);

        $this->loadModel('Countries');
        $this->loadModel('States');
        $countries = $this->Countries->find('list');
        $state = $this->States->find('list');
        $city = $this->Projects->Cities->find('list');
        $this->set(compact('project', 'countries', 'manager', 'client', 'selectedarr', 'state', 'city'));
        $this->set('_serialize', ['project']);
    }
    

    /**
     * View method  */
    public function view($id = null) {
		//debug($id);die;
        $project = $this->Projects->get($id, [
            'contain' => ['Cities', 'Cities.States', 'Cities.States.Countries', 'Clients', 'ProjectUsers', 'ProjectUsers.Users']
        ]);
        $project->started_on=$project->started_on->format('d-m-Y');
        $project->completed_on=$project->completed_on->format('d-m-Y'); 
        $this->loadModel('Payments');
        $payment = $this->Payments
                ->find('all')
                ->where(['project_id =' => $id]);
       $paymentsum=0; $j=0;
       foreach ($payment as $key => $data) {
	      $paymentsum=$paymentsum+$data['amount'];
	      $j++;
	    }
	   //debug($paymentsum);die;
     $this->set(compact('project','paymentsum', $project,$paymentsum));
        $this->set('_serialize', ['project']);
    }

    /**
     * managerProject method
     * 
     * 
     */
    public function managerProject() {
        $this->viewBuilder()->layout('admin_layout');
    }
    
   
   

    /**
     * SaveDetails method 
     * save details when we edit
     */
    public function saveDetails() {
	
        $project = $this->Projects->get($this->request->data['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dateForMysql = date('Y-m-d', strtotime($this->request->data['started_on']));
            $this->request->data['started_on'] = $dateForMysql;
            $dateForMysql1 = date('Y-m-d', strtotime($this->request->data['completed_on']));
            $this->request->data['completed_on'] = $dateForMysql1;
            $project = $this->Projects->patchEntity($project, $this->request->Data());
        }
        $errors = [];
        if (!$project->errors()) {
            $result = $this->Projects->save($project);
            if ($result) {
                $this->loadModel('ProjectUsers');
                $userDetails = $this->ProjectUsers
                        ->find('all')
                        ->where(['project_id =' => $project->id]);

                foreach ($userDetails as $detail) {
                    $this->ProjectUsers->delete($detail);
                }
                foreach ($this->request->data['manager'] as $data) {
                    $managerarr = [
                        'user_id' => $data,
                        'project_id' => $result->id    
                    ];

                    $managerDetails = $this->Projects->ProjectUsers->newEntity();
                    $managerDetails = $this->Projects->ProjectUsers->patchEntity($managerDetails, $managerarr);

                    if ($this->Projects->ProjectUsers->save($managerDetails)) {
						Log::write('info', 'Project' ." ".$result->project_name." ".'edited sucessfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                        $arr['success'] = true;
                        $arr['message'] = 'The project has been saved.';
                    }
                }
            } else {
                $arr['success'] = false;
                $arr['message'] = 'The project could not be saved. Please, try again.';
            }
        } else {
            $errors = implode('-> ', array_values(Hash::flatten($project->errors())));
            $errors = '-> ' . $errors;
            $arr['success'] = false;
            $arr['message'] = $errors;
        }
        echo json_encode($arr);
        die;
    }

    /**
     * AddDetails method 
     */
    public function addDetails() {
	   if ($this->request->data['manager'] == '') {
            $arr['success'] = false;
            $arr['message'] = 'please select atleast one.';
        } else {
            $project = $this->Projects->newEntity();
            $dateForMysql = date('Y-m-d', strtotime($this->request->data['started_on']));
            $this->request->data['started_on'] = $dateForMysql;
            $dateForMysql1 = date('Y-m-d', strtotime($this->request->data['completed_on']));
            $this->request->data['completed_on'] = $dateForMysql1;
            $project = $this->Projects->patchEntity($project, $this->request->data);
            $errors = [];
            if (!$project->errors()) {
                $result = $this->Projects->save($project);
                if ($result) {
                    foreach ($this->request->data['manager'] as $data) {
                        $managerarr = [
                            'user_id' => $data,
                            'project_id' => $result->id
                        ];
                        $managerDetails = $this->Projects->ProjectUsers->newEntity();
                        $managerDetails = $this->Projects->ProjectUsers->patchEntity($managerDetails, $managerarr);
                        if ($this->Projects->ProjectUsers->save($managerDetails)) {
							Log::write('info', 'Project' ." ".$result->project_name." ".'added sucessfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                            $arr['success'] = true;
                            $arr['message'] = 'The project has been saved.';
                        }
                    }
                } else {
                    $arr['success'] = false;
                    $arr['message'] = 'The project could not be saved. Please, try again.';
                }
            } else {
                $errors = implode('-> ', array_values(Hash::flatten($project->errors())));
                $errors = '-> ' . $errors;
                $arr['success'] = false;
                $arr['message'] = $errors;
            }
        }
        echo json_encode($arr);
        die;
    }
    
    /**
     * getProjects method 
     */

    public function getProjects() {
        $aColumns = array('projects.project_name',
            'user.first_name',
            'countries.country_name',
            'states.state_name',
            'cities.city_name',
            'projects.pincode',
            'projects.started_on',
            'projects.completed_on',
            'projects.estimated_budget',
            'projects.approved_budget',
            'projects.id',
        );
        $sIndexColumn = " projects.id ";
        $sTable = " projects ";
        $sJoinTable = ' 
						INNER JOIN cities cities ON cities.id=projects.city_id
						INNER JOIN states states ON states.id=cities.state_id
						INNER JOIN countries countries ON countries.id=states.country_id
						INNER JOIN users user ON user.id=projects.user_id
         ';
        $sConditions = '';
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        //debug($returnArr);die;
        $j=0;
        foreach($returnArr['aaData'] as $r) {
			$returnArr['aaData'][$j]['started_on']=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j]['completed_on']=date('d-m-Y',strtotime($r['completed_on'])); 
			
			$returnArr['aaData'][$j][6]=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j][7]=date('d-m-Y',strtotime($r['completed_on'])); 
			
			
			$j++;
		} 
			
        
        echo json_encode($returnArr);
        die;
    }

    public function getManagerProjects() {
        $aColumns = array('projects.project_name',
            'user.first_name',
            'countries.country_name',
            'states.state_name',
            'cities.city_name',
            'projects.pincode',
            'projects.started_on',
            'projects.completed_on',
            'projects.estimated_budget',
            'projects.approved_budget',
            'projects.id',
        );
        $sIndexColumn = " projects.id ";
        $sTable = " projects ";
        $sJoinTable = ' 
						INNER JOIN cities cities ON cities.id=projects.city_id
						INNER JOIN states states ON states.id=cities.state_id
						INNER JOIN countries countries ON countries.id=states.country_id
						INNER JOIN project_users project_users ON project_users.project_id=projects.id
						INNER JOIN users user ON user.id=projects.user_id
         ';
        $sConditions = 'project_users.user_id = ' . $this->Auth->user('id');
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        $j=0;
        foreach($returnArr['aaData'] as $r) {
			$returnArr['aaData'][$j]['started_on']=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j]['completed_on']=date('d-m-Y',strtotime($r['completed_on'])); 
			
			$returnArr['aaData'][$j][6]=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j][7]=date('d-m-Y',strtotime($r['completed_on'])); 
			
			
			$j++;
		} 
        echo json_encode($returnArr);
        die;
    }
    
     /**
     * clientProjects method
     */ 
    public function clientProjects() {
        $this->viewBuilder()->layout('admin_layout');
    }
    
    /**
     * clientProjects method
     * create datatable for client project
     */ 
    public function getClientProjects() {
        $aColumns = array('projects.project_name',
            'user.first_name',
            'countries.country_name',
            'states.state_name',
            'cities.city_name',
            'projects.pincode',
            'projects.started_on',
            'projects.completed_on',
            'projects.estimated_budget',
            'projects.approved_budget',
            'projects.id',
        );
        $sIndexColumn = " projects.id ";
        $sTable = " projects ";
        $sJoinTable = ' 
						INNER JOIN cities cities ON cities.id=projects.city_id
						INNER JOIN states states ON states.id=cities.state_id
						INNER JOIN countries countries ON countries.id=states.country_id
						INNER JOIN users user ON user.id=projects.user_id
         ';
        $sConditions = 'projects.user_id = ' . $this->Auth->user('id');
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        $j=0;
        foreach($returnArr['aaData'] as $r) {
			$returnArr['aaData'][$j]['started_on']=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j]['completed_on']=date('d-m-Y',strtotime($r['completed_on'])); 
			
			$returnArr['aaData'][$j][6]=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j][7]=date('d-m-Y',strtotime($r['completed_on'])); 
			
			
			$j++;
		} 
        echo json_encode($returnArr);
        die;
    }
   /**
     * managerAllProjects method
     * 
     * 
     */
    public function managerAllProjects($id) {
		
        $this->viewBuilder()->layout('');
        $this->set(compact('id', $id));
       
    }
    
    
    /**
     * getManagerAllProjects method
     * create datatable to see all projects assigned to single manager.
     * 
     */
    
     public function getManagerAllProjects($userId) {
	  $aColumns = array('projects.project_name',
            'user.first_name',
            'countries.country_name',
            'states.state_name',
            'cities.city_name',
            'projects.pincode',
            'projects.started_on',
            'projects.completed_on',
            'projects.estimated_budget',
            'projects.approved_budget',
            
        );
        $sIndexColumn = " projects.id ";
        $sTable = " projects ";
        $sJoinTable = ' 
						INNER JOIN cities cities ON cities.id=projects.city_id
						INNER JOIN states states ON states.id=cities.state_id
						INNER JOIN countries countries ON countries.id=states.country_id
						INNER JOIN project_users project_users ON project_users.project_id=projects.id
						INNER JOIN users user ON user.id=projects.user_id
         ';
        $sConditions = 'project_users.user_id =' . $userId;
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        $j=0;
        foreach($returnArr['aaData'] as $r) {
			$returnArr['aaData'][$j]['started_on']=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j]['completed_on']=date('d-m-Y',strtotime($r['completed_on'])); 
			
			$returnArr['aaData'][$j][6]=date('d-m-Y',strtotime($r['started_on']));
			$returnArr['aaData'][$j][7]=date('d-m-Y',strtotime($r['completed_on'])); 
			
			
			$j++;
		} 
        echo json_encode($returnArr);
        die;
    }

}
