<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 */
class ItemsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
    }

    public function isAuthorized($user = null) {
        $adminArr = array('index', 'index1', 'add', 'saveDetails','getDetails' ,'itemdetails' ,'archive','edit','editDetails' ,'getItems');
        $mangerArr = array('getDetails' , 'managerIndex' , 'allItems','add' ,'saveDetails' ,'edit' ,'editDetails','getItems');
        $clientArr = array();
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
		//debug($this->request);
        $this->viewBuilder()->layout('admin_layout');
       
    }
    
     public function getItems() {
		//debug($this->request);
        $this->viewBuilder()->layout('admin_layout');
        $itemTypeId = $this->request->query['itemTypeId'];
      //  debug($itemTypeId);die;
        $items = $this->Items->find('list')->where(['item_type_id' => $itemTypeId])->toArray();
      /// debug(json_encode($items));die;
        echo json_encode($items);
        die;
       
    }
    
     /**
     * Index1 method
     * Create datatble displaying all items
     *
     */
    public function index1() {
        $aColumns = array(
            'item_types.item_types_name',
            'items.item_name',
            'taxes.tax_value',
            'users.username',
            'items.archive',
            'items.id',
        );
        $sIndexColumn = " items.id ";
        $sTable = " items ";
        $sJoinTable = 'INNER JOIN taxes taxes ON taxes.id=items.tax_id INNER JOIN users users ON users.id=items.user_id INNER JOIN item_types item_types ON item_types.id=items.item_type_id';
        $sConditions = '';
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        echo json_encode($returnArr);
        die;
    }

    /**add method
     * 
     */
    public function add() {
        $item = $this->Items->newEntity();
        if ($this->request->is('post')) {
            $item = $this->Items->patchEntity($item, $this->request->data);
            if ($this->Items->save($item)) {
                $this->Flash->success(__('The item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The item could not be saved. Please, try again.'));
        }
        $taxes = $this->Items->Taxes->find('list')->where(['Taxes.archive'=>0]);
        $this->loadModel('ItemTypes')->displayField('item_types_name');
        $itemTypes = $this->ItemTypes->find('list');
        
        $this->set(compact('item', 'taxes' ,'itemTypes'));
        
        $this->set('_serialize', ['item']);
    }


     /**
     * saveDetails method
     * save detail after add
     */
    public function saveDetails() {
        $item = $this->Items->newEntity();
        $item = $this->Items->patchEntity($item, $this->request->data);
       
        $item->user_id = $this->Auth->user('id');
        // debug( $this->Items->save($item));die;
        if ($this->Items->save($item)) {

            $arr['success'] = true;
            $arr['message'] = 'The item has been saved.';
        } else {
            $arr['success'] = false;
            $arr['message'] = 'The item could not be saved. Please, try again.';
        }
        echo json_encode($arr);
        
       die;
    }
    
    /**
     * Edit method
     *
     */
    public function edit($id) {
		
       $item = $this->Items->get($id, [
            'contain' => ['Users' ]
        ]);
       // debug($item->item_type_id);
        $taxes = $this->Items->Taxes->find('list')->where(['Taxes.archive'=>0]);
        $this->loadModel('ItemTypes')->displayField('item_types_name');
        $itemTypes = $this->ItemTypes->find('list');
        $items = $this->Items->find('list')->where(['item_type_id' => $item->item_type_id])->toArray();
      //  debug($items);die;
        $this->set(compact('item','taxes','id' ,'itemTypes' ,'items'));
        $this->set('_serialize', ['item']);
    }
    
    /**
     * editDetails method
     * save detail after editing
     */
    
    public function editDetails() {
	    // debug($this->request->data['item_type_id']);die;
         $item = $this->Items->get($this->request->data['id']);
         $item = $this->Items->patchEntity($item, $this->request->data);
         if ($this->Items->save($item)) {
            $arr['success'] = true;
            $arr['message'] = 'The item has been saved.';
        } else {
            $arr['success'] = false;
            $arr['message'] = 'The item could not be saved. Please, try again.';
        }
        echo json_encode($arr);
        die;
    }

      /**
     * getDetails method
     *
     */
    public function getDetails($id) {
	  $this->viewBuilder()->layout('admin_layout');
      $itemsDetails = $this->Items->get($id, [
                               'contain' => ['Taxes']
             ]); 
      $taxValue['tax_value']=  $itemsDetails->tax->tax_value;
         if($taxValue!=[])
         {  $taxValue['success'] = true;
			
			 }
			 else
			 {
				 $taxValue['success'] = false;
			 }
			 echo json_encode($taxValue);
        die;
    }
    
    /*archive function
     * 
     */
     public function archive($id) {
	//debug($id);
        $item = $this->Items->get($id);
      
        if($item->archive==0){
            $item->archive=1;
        }else{
             $item->archive=0;
        }
        if ($this->Items->save($item)) {
                $this->Flash->success(__('The tax archive status has been updated.'));

                
            }else{
            $this->Flash->error(__('The tax could not be saved. Please, try again.'));
        
            }
            return $this->redirect(['action' => 'index']);
    }
    
     /**
     * managerIndex method
     *
     * @return \Cake\Network\Response|null
     */
    public function managerIndex() {
        $this->viewBuilder()->layout('admin_layout');
    }
    
    
     /**
     * managerIndex method
     * Create datatble displaying all items
     *
     */
    public function allItems() {
        $aColumns = array(
            'item_types.item_types_name',
            'items.item_name',
            'taxes.tax_value',
            'users.username',
            'items.archive',
            'items.id',
           
        );
        $sIndexColumn = " items.id ";
        $sTable = " items ";
        $sJoinTable = 'INNER JOIN taxes taxes ON taxes.id=items.tax_id INNER JOIN users users ON users.id=items.user_id INNER JOIN item_types item_types ON item_types.id=items.item_type_id';
        $sConditions = '';
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        echo json_encode($returnArr);
        die;
    }
   

}
