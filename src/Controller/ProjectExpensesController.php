<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
use Cake\I18n\Date;
use Cake\Log\Log;
use  App\Controller\AuthComponent;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class ProjectExpensesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
         $this->loadComponent('Flash');
          $this->loadComponent('RequestHandler');
    }

    public function isAuthorized($user = null) {
        $adminArr = array('add', 'edit', 'view', 'addDetails', 'editDetails', 'delete', 'getProjectExpenses' , 'generateInvoice' ,'view1','test');
        $mangerArr = array('add', 'edit', 'view', 'addDetails', 'editDetails', 'delete', 'getProjectExpenses', 'generateInvoice','view1','test');
        $clientArr = array('generateInvoice','viewPdf');
        ;
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
        return true;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        $this->viewBuilder()->layout('admin_layout');
     
    }

    /**
     * Add method
     * add project expenses 
     */
    public function add($id) {
        $projectExpenses = $this->ProjectExpenses->newEntity();
        $this->loadModel('Items');
        $item = $this->Items->find('list')->where(['Items.archive'=>0]);
        $this->loadModel('Taxes');
        $tax = $this->Items->Taxes->find('list',array('keyField'=>'tax_value','valueField'=>'tax_value'))->where(['Taxes.archive'=>0]);
     //  debug(  $tax );die;
        $this->loadModel('Projects');
        $project=$this->Projects->find('all')
                                ->where(['id'=>$id]);
        $this->loadModel('ItemTypes')->displayField('item_types_name');
        $itemTypes = $this->ItemTypes->find('list');
        $this->set(compact('projectExpenses', 'item', 'tax', 'id' ,'project' ,'itemTypes'));
        $this->set('_serialize', ['projectExpenses']);
    }
    

    /**
     * Edit method
     *edit project expenses
     *
     */
   public function edit($id,$projectID) {
	  $projectExpenses = $this->ProjectExpenses->get($id, [
            'contain' => ['Projects', 'Items', 'Items.Taxes']
        ]);
     ///   debug($projectExpenses->item_id);die;
        $projectExpenses->added_on=$projectExpenses->added_on->format('d-m-Y');
        $this->loadModel('Items');
        $this->loadModel('Taxes');
        $item = $this->Items->find('list')->where(['item_type_id' => $projectExpenses->item_type_id])->toArray();
       
        $tax = $this->Items->Taxes->find('list')->where(['Taxes.archive'=>0]);
        $this->loadModel('Projects');
        $project=$this->Projects->find('all')
                                ->where(['id'=>$projectID])->toArray();
                                 $this->loadModel('ItemTypes')->displayField('item_types_name');
        $itemTypes = $this->ItemTypes->find('list');
        $this->set(compact('projectExpenses', 'item', 'tax', 'id','project','itemTypes'));
        $this->set('_serialize', ['projectExpenses']);
    }
    

    /**
     * View method
     * view project expenses
     * 
     */
    public function view($id) {
        $this->loadModel('ProjectExpenses');
        $projectExpense = $this->ProjectExpenses
                ->find('all')
                ->where(['project_id =' => $id]);
        $this->set(compact('projectExpense', 'id', $projectExpense, $id));
        $this->set('_serialize', ['projectExpense']);
    }

    /**
     * addDetails method
     * add project expenses 
     */ 
    public function addDetails() {
		
		$this->loadModel('Projects');
		$project = $this->Projects
                              ->find('all')
                              ->select(['project_name'])
                              ->where(['id =' => $this->request->data['project_id']])
                              ->toArray() ;
        foreach($project as $project1)
        {
		   $project1->project_name;
	    }
        $this->loadModel('Ledgers');
        $this->loadModel('Items');
        $dateForMysql = date('Y-m-d', strtotime($this->request->data['added_on']));
        $this->request->data['added_on'] = $dateForMysql;
       
        if (isset($this->request->data['id'])) {
            $projectExpense = $this->ProjectExpenses->get($this->request->data['id']);
            $ledger = $this->Ledgers->get($projectExpense->ledger_id);
        } else {
            $projectExpense = $this->ProjectExpenses->newEntity();
            $ledger = $this->Ledgers->newEntity();
        }
        $this->request->data['user_id'] = $this->Auth->user('id');
        $ledgerDetails['project_id'] = $this->request->data['project_id'];
        $ledgerDetails['transaction_type_id'] = 1;
        $ledgerDetails['particulars'] = $this->request->data['total_price'] . " Spend on project " . $project1->project_name;
        $ledgerDetails['amount'] = $this->request->data['total_price'];
        $ledgerDetails['transaction_date'] = $this->request->data['added_on'];
        if ($this->request->data['item_type'] == 1) {
            if (isset($this->request->data['add_item']) && $this->request->data['add_item'] != '') {
                $item = $this->Items->newEntity();
                $ItemDetails['item_name'] = $this->request->data['add_item'];
                $ItemDetails['tax_id'] = $this->request->data['tax_id'];
                $ItemDetails['user_id'] = $this->Auth->user('id');
                $item = $this->Items->patchEntity($item, $ItemDetails);
                $itemDetail = $this->Items->save($item);
                if ($itemDetail) {
                    $this->request->data['item_id'] = $itemDetail->id;
                } else {
                    $arr['success'] = false;
                    $arr['message'] = 'The project expenses could not be saved. Please, try again.';
                    echo json_encode($arr);
                    die;
                }
            } else {
                $arr['success'] = false;
                $arr['message'] = 'The project expenses could not be saved. Please, try again.';
                echo json_encode($arr);
                die;
            }
        }
        $ledger = $this->Ledgers->patchEntity($ledger, $ledgerDetails);
        $ledgerresult = $this->Ledgers->save($ledger);
        if ($ledgerresult) {
            $this->request->data['ledger_id'] = $ledgerresult->id;
            $projectExpense = $this->ProjectExpenses->patchEntity($projectExpense, $this->request->data);
            $errors = [];
            if (!$projectExpense->errors()) {
				$expenseResult=$this->ProjectExpenses->save($projectExpense);
				
                if ($expenseResult) {
				  $invoicedata=$this->generateInvoice($expenseResult->id);
			      if($invoicedata){ 
					   if (isset($this->request->data['id'])) {
                            Log::write('info', 'Project Expenses of project '. " ".$project1->project_name." ".'edited sucessfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                        } else {
                            Log::write('info', 'New Project Expenses of project '. " ".$project1->project_name." ". 'added sucessfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                           }
					 
					   $arr['success'] = true;
                       $arr['message'] = 'The project expenses has been saved.';
                        
				    } else{
					    $arr['success'] = false;
                        $arr['message'] = 'The project expenses could not be saved. Please, try again.';
				       }
                  
                } else {
                    $arr['success'] = false;
                    $arr['message'] = 'The project expenses could not be saved. Please, try again.';
                }
            } else {
                $errors = implode('<br>-> ', array_values(Hash::flatten($projectExpense->errors())));
                $errors = '-> ' . $errors;
                $arr['success'] = false;
                $arr['message'] = $errors;
            }
      } else {
            $arr['success'] = false;
            $arr['message'] = 'The project expenses could not be saved. Please, try again.';
        }

        echo json_encode($arr);
        die;
    }

    
    /**
     * Delete method
     *
     * 
     */
    public function delete($id) {

        $projectExpense = $this->ProjectExpenses->get($id);
        $this->loadModel('Ledgers');
        $ledger = $this->Ledgers->get($projectExpense->ledger_id);
        if ($this->Ledgers->delete($ledger)) {
            if ($this->ProjectExpenses->delete($projectExpense)) {
                $arr['success'] = true;
                $arr['message'] = 'The project expense has been deleted.';
            }
        } else {
            $arr['success'] = false;
            $arr['message'] = 'The project expense could not be deleted. Please, try again.';
        }
        echo json_encode($arr);
        die;
    }

    public function getProjectExpenses($id) {

        $aColumns = array(
            'projects.project_name',
            'item_types.item_types_name',
            'items.item_name',
            'project_expenses.quantity',
            'project_expenses.price_per_unit',
            'taxes.tax_value',
            'project_expenses.tax_amount',
            'project_expenses.total_price',
            'project_expenses.added_on',
            'users.first_name',
            'project_expenses.id'
        );
        $sIndexColumn = " project_expenses.id ";
        $sTable = " project_expenses ";
        $sJoinTable = ' 
				        INNER JOIN projects projects ON projects.id=project_expenses.project_id
					    INNER JOIN items items ON items.id=project_expenses.item_id
				        INNER JOIN users users ON users.id=project_expenses.user_id
				        INNER JOIN taxes taxes ON taxes.id=items.tax_id
				        INNER JOIN item_types item_types ON item_types.id=project_expenses.item_type_id
				        
         ';
        $sConditions = 'project_expenses.project_id=' . $id;
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
     //  debug($returnArr);die;
        $j=0;
        foreach($returnArr['aaData'] as $return)
        {
			$returnArr['aaData'][$j]['added_on']=date('d-m-Y',strtotime($return['added_on']));
			$returnArr['aaData'][$j][8]=date('d-m-Y',strtotime($return['added_on']));
			$j++;
		}
        echo json_encode($returnArr);
        die;
    }
    
    
    public function generateInvoice($invoice)
    {
		
		$projectExpense=$this->ProjectExpenses->find()->where(['id'=>$invoice])->first();
		$this->loadModel('Projects');
		$project=$this->Projects->find()->where(['id'=>$projectExpense->project_id])->first();
        $this->loadModel('Users');
		$user=$this->Users->find()->where(['id'=>$project->user_id])->first();
	    $this->loadModel('Cities');
		$city=$this->Cities->find()->where(['id'=>$user->city_id])->first();
		$this->loadModel('States');
		$state=$this->States->find()->where(['id'=>$city->state_id])->first();
	    $this->loadModel('Countries');
		$country=$this->Countries->find()->where(['id'=>$state->country_id])->first();
	    $this->loadModel('Invoices');
		$invoicetable=$this->Invoices->find()->where(['project_expense_id'=>$invoice])->first();
        if (!Empty($invoicetable)) {
            $invoicetable1 = $this->Invoices->get($invoicetable->id);
        } else {
             $invoicetable1 = $this->Invoices->newEntity();
          }
        $ProjectExpenseDetail=$this->ProjectExpenses->find()->where(['ProjectExpenses.id'=>$invoice])->contain(['Projects', 'Users','Projects.Cities', 'Projects.Cities.States', 'Projects.Cities.States.Countries','Items','Items.Taxes'])->first();
	    $invoiceDetails['project_expense_id']=$ProjectExpenseDetail->id;
		$invoiceDetails['added_on']=$ProjectExpenseDetail->added_on;
		$invoiceDetails['content']['clientname']=$user->username;
		$invoiceDetails['content']['client_address']=$city->city_name .' ' .$state->state_name . ' ' .$country->country_name;
	    $invoiceDetails['content']['username']=$ProjectExpenseDetail->user->username;
		$invoiceDetails['content']['project_name']=$ProjectExpenseDetail->project->project_name;
        $invoiceDetails['content']['project_address'] = $ProjectExpenseDetail->project->city->city_name . ' ' . $ProjectExpenseDetail->project->city->state->state_name . ' ' . $ProjectExpenseDetail->project->city->state->country->country_name;
		$invoiceDetails['content']['item_name']=$ProjectExpenseDetail->item->item_name;
		$invoiceDetails['content']['quantity']=$ProjectExpenseDetail->quantity;
		$invoiceDetails['content']['price_per_unit']=$ProjectExpenseDetail->price_per_unit;
		$invoiceDetails['content']['tax_percentage']=$ProjectExpenseDetail->item->tax->tax_value;
		$invoiceDetails['content']['tax_amount']=$ProjectExpenseDetail->tax_amount;
		$invoiceDetails['content']['total_price']=$ProjectExpenseDetail->total_price;
        $invoicedata=json_encode($invoiceDetails['content']);
	    $invoiceDetails['content']=$invoicedata;
	    $invoiceContent = $this->Invoices->patchEntity($invoicetable1, $invoiceDetails);
	    $invoiceResult=$this->Invoices->save($invoiceContent);
	   if ($invoiceResult) {
			    return true;
		 } else {
				   return false;
				}
				
	}
	/**
     * view1 method
     *
     * 
     */
   public function view1()
        {  
         $id=$this->request->query['ExpenseID'];
		 $this->loadModel('Invoices');
		 $invoicetable=$this->Invoices->find()->where(['project_expense_id'=>$id])->first();
         $invoiceId=$invoicetable->id;
		 $invoicedata=json_decode($invoicetable->content);
		 $this->viewBuilder()->options([
                'pdfConfig' => [
                    'orientation' => 'portrait',
                   'filename' => 'Invoice_' . $invoiceId.'.pdf',
                   'download' => (bool)$this->request->query('download')
                ]
            ]);
             $this->set(compact('invoicedata', 'invoicetable'));
       }
         
      
}

 
