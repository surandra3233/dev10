<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Reports Controller
 *
 * @property \App\Model\Table\ReportsTable $Reports
 */
class ReportsController extends AppController {

    public function isAuthorized($user = null) {
        $adminArr = array('index', 'overAllReport', 'weeklyReport', 'itemsReport' ,'getProjectData');
        $mangerArr = array('index', 'overAllReport', 'weeklyReport', 'itemsReport','getProjectData');
        $clientArr = array('index', 'overAllReport', 'weeklyReport', 'itemsReport','getProjectData');
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }

    /**index method
     * 
     */
    public function index() {
        $this->viewBuilder()->layout('admin_layout');
        if($this->Auth->user('role_id')==1){
        $projects = $this->Reports->Projects->find('list');
        }else if($this->Auth->user('role_id')==2){
            $this->loadModel('ProjectUsers');
            $project=$this->ProjectUsers->find('all')->where(['ProjectUsers.user_id'=>$this->Auth->user('id')])->contain(['Projects']);
             foreach($project as $p){
                $projects[$p->project->id] =$p->project->project_name;
             }
        }else{
          $projects = $this->Reports->Projects->find('list')->where(['user_id'=>$this->Auth->user('id')]);
        }
      
        $report_type = $this->Reports->ReportTypes->find('list');
        $this->set(compact('projects', 'report_type'));
    }
    
    
    /**index method
     * 
     */
    public function getProjectData() {
		//debug($this->request);die;
        $this->viewBuilder()->layout('admin_layout');
        $projectId = $this->request->query['projectId'];
    //  debug($projectId);die;
        $this->loadModel('Projects');
        $projects = $this->Projects->find()->select(['started_on','completed_on'])->where(['id' => $projectId])->first()->toArray();
        //debug($projectId);
        //debug($projects); die;
        $projects['started_on']=date('d-m-Y',strtotime($projects['started_on']));
        $projects['completed_on']=date('d-m-Y',strtotime($projects['completed_on']));
       // debug($projects);die; 
      //  debug(json_encode($projects));die;
        echo json_encode($projects);
        die;
		
	}

    public function overAllReport() {
	//	debug($this->request->query['startDate']);
		//debug($this->request->query['endDate']);die;
	    $project = $this->request->query['project'];
        $reportType = $this->request->query['reportType'];
        $fromDate = $this->request->query['startDate'];
        $fromDate=date('Y-m-d',strtotime($fromDate));
        $toDate = $this->request->query['endDate'];
        $toDate=date('Y-m-d',strtotime($toDate));
        
        $this->loadModel('Projects');
        
		$projectDetails = $this->Projects->find()->where(['Projects.id' => $project])->contain(['Cities', 'Cities.States', 'Cities.States.Countries'])->first();
	     if($projectDetails)
         {
			$arr['projectName'] = $projectDetails->project_name;
			$arr['estimated_budget'] = $projectDetails->estimated_budget;
			$arr['approved_budget'] = $projectDetails->approved_budget;
			$arr['Location'] = $projectDetails->city->state->country->country_name . " " . $projectDetails->city->state->state_name . " " . $projectDetails->city->city_name;
	     }
         else
	     {
		    $arr['projectName'] = '';
			$arr['estimated_budget'] = '';
			$arr['approved_budget'] = '';
			$arr['Location'] = '';
	     }
        $this->loadModel('Ledgers');
        $ledgerDetails1 = $this->Ledgers->find('all')->where(['project_id' => $project])->toArray();
        $ledgerDetails = $this->Ledgers->find('all')->where(['project_id' => $project])
                                                  ->andWhere(['transaction_date >='=>$fromDate, 'transaction_date <='=>$toDate]);
        $ledgerDetails2 = $this->Ledgers->find('all')->where(['project_id' => $project])
                                                  ->andWhere(['transaction_date ='=>$fromDate]);
        $ledgerDetails3 = $this->Ledgers->find('all')->where(['project_id' => $project])
                                                  ->andWhere(['transaction_date ='=>$toDate]);
    // debug($ledgerDetails3->toArray());die;      
        $budgetAlloted = 0;
        $amountSpend = 0;
        if ($reportType == 1) {
	       if(($this->request->query['startDate']=='')&&($this->request->query['endDate']=='')){
			   foreach ($ledgerDetails1 as $details) {
                 if ($details->transaction_type_id == 2) {
                    $budgetAlloted = $budgetAlloted + $details->amount;
                 } else if ($details->transaction_type_id == 1) {
                    $amountSpend = $amountSpend + $details->amount;
                 } 
               } 
              $arr['budgetAlloted'] = $budgetAlloted;
              $arr['amountSpend'] = $amountSpend;
           } elseif($this->request->query['endDate']==''){
              foreach ($ledgerDetails2 as $details) {
                 if ($details->transaction_type_id == 2) {
                    $budgetAlloted = $budgetAlloted + $details->amount;
                 } else if ($details->transaction_type_id == 1) {
                    $amountSpend = $amountSpend + $details->amount;
                 } 
               } 
              $arr['budgetAlloted'] = $budgetAlloted;
              $arr['amountSpend'] = $amountSpend;
              
       } elseif($this->request->query['startDate']==''){
           foreach ($ledgerDetails3 as $details) {
                 if ($details->transaction_type_id == 2) {
                    $budgetAlloted = $budgetAlloted + $details->amount;
                 } else if ($details->transaction_type_id == 1) {
                    $amountSpend = $amountSpend + $details->amount;
                 } 
               } 
              $arr['budgetAlloted'] = $budgetAlloted;
              $arr['amountSpend'] = $amountSpend;
        
          } else {
             foreach ($ledgerDetails as $details) {
               if ($details->transaction_type_id == 2) {
                    $budgetAlloted = $budgetAlloted + $details->amount;
                } else if ($details->transaction_type_id == 1) {
                    $amountSpend = $amountSpend + $details->amount;
                }
             } 
            $arr['budgetAlloted'] = $budgetAlloted;
            $arr['amountSpend'] = $amountSpend;
         }  
       }
       $this->set(compact('arr'));
    }

  /*  public function weeklyReport() {
        $project = $this->request->query['project'];
        $reportType = $this->request->query['reportType'];
        $this->loadModel('Projects');
        $projectDetails = $this->Projects->find()->where(['Projects.id' => $project])->contain(['Cities', 'Cities.States', 'Cities.States.Countries'])->first();
        $arr['projectName'] = $projectDetails->project_name;
        $arr['estimated_budget'] = $projectDetails->estimated_budget;
        $arr['approved_budget'] = $projectDetails->approved_budget;
        $arr['Location'] = $projectDetails->city->city_name . " " . $projectDetails->city->state->state_name . " " . $projectDetails->city->state->country->country_name;
        $arr['detail'][0]['amountSpend']=0;
        $this->loadModel('Ledgers');
        $time = Time::now();
       // debug($time);
        $ledgerDetails = $this->Ledgers->find('all')->where(['project_id' => $project]);
        if ($reportType == 2) {
            $startDate = $projectDetails->started_on;
            $week = $startDate->addDays(7);
             
            $i = 0;
           
            while ($week <= $time) {
			//	debug($time);
			//	debug($week );
                 if($time<=$projectDetails->completed_on){
                $amountSpend = 0;
                foreach ($ledgerDetails as $details) {
					//debug($details);die;
                    if ($details->transaction_date <= $week && $details->transaction_date > $week->subDays(7)) {
                        if ($details->transaction_type_id == 1) {
                            $amountSpend = $amountSpend + $details->amount;
                        }
                    }
                }
                $arr['detail'][$i]['amountSpend'] = $amountSpend;
                $week = $week->addDays(7);
                $i++;
                 }
            }
            
        }
      //  debug($arr);die;
        $this->set(compact('arr'));
    }
*/
    public function itemsReport() {
        $project = $this->request->query['project'];
        $reportType = $this->request->query['reportType'];
        $fromDate = $this->request->query['startDate'];
        $fromDate=date('Y-m-d',strtotime($fromDate));
        $toDate = $this->request->query['endDate'];
        $toDate=date('Y-m-d',strtotime($toDate));
     //  debug($fromDate);
    //  debug($toDate);  die;  
        $this->loadModel('Projects');
        $projectDetails = $this->Projects->find()->where(['Projects.id' => $project])->contain(['Cities', 'Cities.States', 'Cities.States.Countries'])->first();
         if($projectDetails){
			$arr['projectName'] = $projectDetails->project_name;
			$arr['estimated_budget'] = $projectDetails->estimated_budget;
			$arr['approved_budget'] = $projectDetails->approved_budget;
			$arr['Location'] = $projectDetails->city->city_name . " " . $projectDetails->city->state->state_name . " " . $projectDetails->city->state->country->country_name;
	    } 
	    else
	    {
		    $arr['projectName'] = '';
			$arr['estimated_budget'] = '';
			$arr['approved_budget'] = '';
			$arr['Location'] = '';
	    }
        if ($reportType == 3) {
			  $this->loadModel('ProjectExpenses');
              if(($this->request->query['startDate']=='')&&($this->request->query['endDate']=='')){
                  $expenceDetails1 = $this->ProjectExpenses->find('all')->where(['project_id' => $project])->contain(['Items']);
                // debug($expenceDetails->toArray());die;
                  $arr['expence'] = $expenceDetails1;
               }elseif($this->request->query['endDate']==''){
                  $expenceDetails2 = $this->ProjectExpenses->find('all')->where(['project_id' => $project])
                                                                     ->andWhere(['added_on ='=>$fromDate])
                                                                     ->contain(['Items']);
                // debug($expenceDetails->toArray());die;
                  $arr['expence'] = $expenceDetails2;
                  
               }elseif($this->request->query['startDate']==''){
				  $expenceDetails3 = $this->ProjectExpenses->find('all')->where(['project_id' => $project])
                                                                     ->andWhere(['added_on ='=>$toDate])
                                                                     ->contain(['Items']);
                // debug($expenceDetails->toArray());die;
                  $arr['expence'] = $expenceDetails3;
				   
               } else {
                  $expenceDetails = $this->ProjectExpenses->find('all')->where(['project_id' => $project])
                                                                   ->andWhere(['added_on >='=>$fromDate, 'added_on <='=>$toDate])
                                                                   ->contain(['Items']);
            // debug($expenceDetails->toArray());die;
                  $arr['expence'] = $expenceDetails;
            } 
       }
       $this->set(compact('arr'));
    }

}
