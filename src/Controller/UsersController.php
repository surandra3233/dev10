<?php

namespace App\Controller;
use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Log\Log;
use  App\Controller\AuthComponent;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
 
class UsersController extends AppController {
 
    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
        $this->loadComponent('Captcha.Captcha');
    }

    public function isAuthorized($user = null) {
        $adminArr = array('add', 'index', 'index1', 'edit', 'view', 'dashboard', 'addDetails', 'editDetails' , 'myprofile', 'passwordedit', 'login1', 
                           'sendResetEmail', 'reset', 'userdetails','logDetail','getLogs' ,'viewlogDetail','getManagers');
        $mangerArr = array('managerDashboard', 'myprofile', 'passwordedit');
        $clientArr = array('clientDashboard', 'myprofile', 'passwordedit');
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }
     public function test() {
		 $this->viewBuilder()->layout('admin_layout');
    }

    /**
     * Index method
     * Method to get all users
     */
    public function index() {
		 $this->viewBuilder()->layout('admin_layout');
    }

    /**
     * Index1 method
     * Create datatble displaying all users
     *
     */
    public function index1() {
        $aColumns = array('users.first_name',
            'users.last_name',
            'users.username',
            'users.phone',
            'users.email',
            'roles.role_name',
            'countries.country_name',
            'states.state_name',
            'cities.city_name',
            'users.pincode',
            'users.id',
        );
        $sIndexColumn = " users.id ";
        $sTable = " users ";
        $sJoinTable = 'INNER JOIN cities cities ON cities.id=users.city_id INNER JOIN states states ON states.id=cities.state_id  INNER JOIN countries countries ON countries.id=states.country_id INNER JOIN roles roles ON roles.id=users.role_id';
        $sConditions = 'users.id !=' . $this->Auth->user('id');
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        echo json_encode($returnArr);
        die;
    }

    /**
     * View method
     * display user details
     */
    public function view($id = null) {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Cities', 'Cities.States', 'Cities.States.Countries']
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     * add new user 
     */
    public function add() {
        $user = $this->Users->newEntity();
        $this->loadModel('Countries');
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $countries = $this->Countries->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles', 'countries'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     * edit a user 
     *
     */
    public function edit($id = null) {
        $user = $this->Users->get($id, [
            'contain' => ['Roles', 'Cities', 'Cities.States', 'Cities.States.Countries']
        ]);
        $this->loadModel('Countries');
        $this->loadModel('States');
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $countries = $this->Countries->find('list');
        $state = $this->States->find('list');
        $city = $this->Users->Cities->find('list');
        $this->set(compact('user', 'roles', 'countries', 'state', 'city'));
        $this->set('_serialize', ['user']);
    }

     /**
     * AddDetails method 
     * save user after adding user details
     */
    public function addDetails() {
		 $user = $this->Users->newEntity();
         $user = $this->Users->patchEntity($user, $this->request->data);
         $errors = [];
         if (!$user->errors()) {
			 $result=$this->Users->save($user);
             if ($result) {
			     Log::write('info', 'New user'." ".$result->first_name." ".$result->last_name." " .' added sucessfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
				 $arr['success'] = true;
                 $arr['message'] = 'The user has been saved.';
             } else {
                 $arr['success'] = false;
                 $arr['message'] = 'The user could not be saved. Please, try again.';
               }
		 } else {
           $errors = implode('<br>-> ', array_values(Hash::flatten($user->errors())));
           $errors = '-> ' . $errors;
           $arr['success'] = false;
           $arr['message'] = $errors;
         }
        echo json_encode($arr);
        die;
	   
    }

   /**
     * editDetails method 
     * save user after editing userdetails
     * 
     */
     public function editDetails() {
	 
		$user = $this->Users->get($this->request->data['id']);
            if ($this->request->data['change_password'] == 1) {
                $this->request->data['password'] = $this->request->data['new_password'];
            }
        $user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'editdetailsUsers']);
		$errors = [];
        if (!$user->errors()) {
           if ($this->Users->save($user)) {
			   if (!isset($this->request->data['id']) || $this->request->data['change_password'] == 1) {
				    $email = new Email();
                    $email->viewVars(array('firstname' => $this->request->data['first_name']));
                    $email->viewVars(array('lastname' => $this->request->data['last_name']));
                    $email->viewVars(array('username' => $this->request->data['username']));
                    $email->viewVars(array('email' => $this->request->data['email']));
                    $email->viewVars(array('pass' => $this->request->data['password']));
                      if ($this->request->data['change_password'] == 1) {
                          $email->template('passwordchange');
                       } else {
                          $email->template('register');
                         }
                    $email->emailFormat('both')
                            ->to($this->request->data['email'])
                            ->from('alerts@accounts.netgen.in')
                            ->subject(sprintf('Welcome %s', $this->request->data['username']));
                    if ($email->send()) {
		                Log::write('info', ' Password of user '. " ".$this->request->data['first_name']." ".$this->request->data['last_name']." ".'edited and mailed sucessfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                        $arr['success'] = true;
                        $arr['message'] = 'The user has been saved.';
                    } else {
                        $arr['success'] = false;
                        $arr['message'] = 'The user could not be saved. Please, try again.';
                     }
				 } else {
				        Log::write('info', 'Details of user '. " ".$this->request->data['first_name']." ".$this->request->data['last_name']." ".'edited sucessfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                        $arr['success'] = true;
                        $arr['message'] = 'The user has been saved.';
                   }
		 } else {
              $arr['success'] = false;
              $arr['message'] = 'The user could not be saved. Please, try again.';
              }
	  } else {
           $errors = implode('<br>-> ', array_values(Hash::flatten($user->errors())));
           $errors = '-> ' . $errors;
           $arr['success'] = false;
           $arr['message'] = $errors;
         }
        echo json_encode($arr);
        die;
	 }
	 
	 
    /**
     * Delete method
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * Log method
     * display log detail
     * 
     */
    public function logDetail($id) {
		$this->set(compact('id',  $id));
       
    }
    
   /**
     * getLogs method
     * Create datatble displaying  user log details     
     * 
     */
     public function getLogs($id) {
	    $aColumns = array('access_logs.message',
            'access_logs.ip',
            'access_logs.created',
            
            'access_logs.id',
        );
        $sIndexColumn = " access_logs.id ";
        $sTable = " access_logs ";
        $sJoinTable = '';
        $sConditions = 'access_logs.user_id=' . $id;
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        echo json_encode($returnArr);
        die;
    }
    
    
    /**
     * viewlogDetail method
     * display complete log detail
     * 
     */
    public function viewlogDetail($id) {
	    $this->loadModel('AccessLogs');                            
        $totallogs = $this->AccessLogs
                           ->find('all')
                           ->where(['id =' => $id])
                           ->toArray() ;
        foreach($totallogs as $logs)
         {    
				// debug($logs->ip);die;
		 }
             
		$this->set(compact('logs',  $logs));
    }

    /**
     * Login method
     * Login for admin , manager and client
     * 
     */
    public function login() {
	   $this->viewBuilder()->layout('login');
       $user = $this->Auth->user();
        if (!empty($this->Auth->user('role_id'))) {
            if ($user['role_id'] == 1) {
                return $this->redirect(['action' => 'dashboard']);
            } elseif ($user['role_id'] == 2) {
                return $this->redirect(['action' => 'managerDashboard']);
            } elseif ($user['role_id'] == 3) {
                return $this->redirect(['action' => 'clientDashboard']);
            } else {
                $this->Flash->error(__('Something Went Wrong, Please Try Again !'));
            }
        }
       if ($this->request->is('POST')) {
		 
		   
	    $ip = getenv('REMOTE_ADDR');
     	 if(isset($_SESSION['failedAttempt'])) {
			if($_SESSION['failedAttempt']>=3) {
				$gRecaptchaResponse = $this->request->data['g-recaptcha-response'];
				//debug($gRecaptchaResponse);die;
				 $captcha = $this->Captcha->check($ip,$gRecaptchaResponse);
					 if($captcha->errorCodes == null) {
						 $user = $this->Auth->identify();
						 if ($user) {
							  $this->Auth->setUser($user);
							  $user = $this->Users->get($this->Auth->user('id'));
							  $user->last_login = Time::now();
							  if ($this->Users->save($user)) {
								  Log::write('info', 'Login Sucessful',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
								  if ($this->Auth->user('role_id') == 1) {
									  return $this->redirect(['action' => 'dashboard']);
								  } else if ($this->Auth->user('role_id') == 2) {
									  return $this->redirect(['action' => 'managerDashboard']);
								  } else {
									  return $this->redirect(['action' => 'clientDashboard']);
								  }
							  } else {
							   $this->Flash->error(__('Something went wrong. Please Try Again'));
								}
						 } else {
							   if(isset($_SESSION['failedAttempt'])) {
								$_SESSION['failedAttempt']=$_SESSION['failedAttempt']+1;
								} else {
								$_SESSION['failedAttempt']=1;
								}
							   $this->Flash->error(__('Username or password is incorrect'));
							   }
					 } else {
						$this->Flash->error(__('Please check your Recaptcha Box.'));
							}
				} else {
							$user = $this->Auth->identify();
						 if ($user) {
							  $this->Auth->setUser($user);
							  $user = $this->Users->get($this->Auth->user('id'));
							  $user->last_login = Time::now();
							  if ($this->Users->save($user)) {
								  Log::write('info', 'Login Sucessful',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
								  if ($this->Auth->user('role_id') == 1) {
									  return $this->redirect(['action' => 'dashboard']);
								  } else if ($this->Auth->user('role_id') == 2) {
									  return $this->redirect(['action' => 'managerDashboard']);
								  } else {
									  return $this->redirect(['action' => 'clientDashboard']);
								  }
							  } else {
							   $this->Flash->error(__('Something went wrong. Please Try Again'));
								}
						 } else {
							   if(isset($_SESSION['failedAttempt'])) {
								$_SESSION['failedAttempt']=$_SESSION['failedAttempt']+1;
								} else {
								$_SESSION['failedAttempt']=1;
								}
							   $this->Flash->error(__('Username or password is incorrect'));
							   }
						}
		} else {
			             $user = $this->Auth->identify();
						 if ($user) {
							  $this->Auth->setUser($user);
							  $user = $this->Users->get($this->Auth->user('id'));
							  $user->last_login = Time::now();
							  if ($this->Users->save($user)) {
								  Log::write('info', 'Login Sucessful',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
								  if ($this->Auth->user('role_id') == 1) {
									  return $this->redirect(['action' => 'dashboard']);
								  } else if ($this->Auth->user('role_id') == 2) {
									  return $this->redirect(['action' => 'managerDashboard']);
								  } else {
									  return $this->redirect(['action' => 'clientDashboard']);
								  }
							  } else {
							   $this->Flash->error(__('Something went wrong. Please Try Again'));
								}
						 } else {
							   if(isset($_SESSION['failedAttempt'])) {
								$_SESSION['failedAttempt']=$_SESSION['failedAttempt']+1;
								} else {
								$_SESSION['failedAttempt']=1;
								}
							  $this->Flash->error(__('Username or password is incorrect'));
							   }
		  }
       }
    } 
    

    /**
     * Logout method
     * 
     * 
     */
    public function logout() {
        $session = $this->request->session();
        $logoutReferer = $session->read('LOG_OUT_REFERER');
        $user=$this->Auth->user('id');
		if($user){	
			Log::write('info', 'Logout',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
		}
        $this->Auth->logout();
        $session->destroy();
        if ($logoutReferer) {
            return $this->redirect($logoutReferer);
        }
        return $this->redirect(['action' => 'login']);
    }

    /**
     * Dashboard method
     * amin dashboard
     * 
     */
    public function dashboard() {
        $this->viewBuilder()->layout('admin_layout');
       
        $this->loadModel('Users');
        $totalUsers = $this->Users->find()->count();
        $this->loadModel('Projects');
        $totalProjects = $this->Projects->find()->count();
        $projects = $this->Projects->find('list', ['limit' => 200]);
        $this->loadModel('Items');
        $totalItems = $this->Items->find()->count();
        $this->loadModel('ReportTypes');
        $report_type = $this->ReportTypes->find('list', ['limit' => 200]);
        $this->loadModel('Taxes');
        $totalTaxes = $this->Taxes->find()->count();
        
        $this->set(compact('totalUsers', 'totalProjects','projects','totalItems','report_type' ,'totalTaxes'));
       
     }
     
     /**
     * managerDashboard method
     * manager dashboard
     * 
     */
  public function managerDashboard() {
        $this->viewBuilder()->layout('admin_layout');
        $this->loadModel('ProjectUsers');                            
        $totalProjects = $this->ProjectUsers
                            ->find('all')
                            ->select(['project_id'])
                            ->where(['user_id =' => $this->Auth->user('id')])
                            ->toArray() ;
        $projectsCount= count($totalProjects);
      //  debug($projectsCount);die;
        $newProject=array(); 
                       
        foreach( $totalProjects as $project)
        {     
			 $newProject[]=$project['project_id'];
			
		}
	  // debug($newProject);
       $this->loadModel('Projects');
       $projects=array();    
       if($newProject)
       {                    
        $projects=$this->Projects->find('list')
                                ->select(['project_name']) 
                                ->where(['id IN'=>$newProject])->toArray(); 
       }
       
       $this->loadModel('Items');
       $totalItems = $this->Items->find()->count();
       $this->loadModel('ReportTypes');
       $report_type = $this->ReportTypes->find('list', ['limit' => 200]);
       $this->set(compact('projectsCount','totalItems','projects','report_type'));
    }
    
    /**
    * clientDashboard method
    * client dashboard
    * 
    */
    public function clientDashboard() {
        $this->viewBuilder()->layout('admin_layout');
        $this->loadModel('Projects');
        $projects = $this->Projects->find('list')->where(['user_id'=>$this->Auth->user('id')])->toArray();
        $projectsCount= count($projects);
        $this->loadModel('ReportTypes');
        $report_type = $this->ReportTypes->find('list', ['limit' => 200]);
        $this->set(compact('projectsCount','projects','report_type'));
    }
 
   
    /**
     * myprofile method 
     * view And edit logged user profile 
     *  
     */
    public function myprofile() {
        $this->viewBuilder()->layout('admin_layout');
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => ['Roles', 'Cities', 'Cities.States', 'Cities.States.Countries']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data, ['validate' => 'editdetailsUsers']);
            if ($this->Users->save($user)) {
				Log::write('info', 'User details edited successfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'myprofile']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->loadModel('Countries');
        $this->loadModel('States');
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $countries = $this->Countries->find('list');
        $state = $this->States->find('list');
        $city = $this->Users->Cities->find('list');
        $this->set(compact('user', 'roles', 'countries', 'state', 'city'));
        $this->set('_serialize', ['user']);
    }

    public function passwordedit() {
        $this->autoRender = false;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->get($this->request->data['id']);
            $newpass['id'] = $this->request->data['id'];
            $newpass['cpassword'] = $this->request->data['cpassword'];
            $newpass['npassword'] = $this->request->data['npassword'];
            $newpass['passwordconfirm'] = $this->request->data['passwordconfirm'];
            $user = $this->Users->patchEntity($user, $newpass, ['validate' => 'changePassword']);
            if (!$user->errors()) {
                $pass['password'] = $this->request->data['npassword'];
                $user = $this->Users->patchEntity($user, $pass, ['validate' => false]);
                if ($this->Users->save($user)) {
					Log::write('info', 'Password edited successfully',['user_id'=>$this->Auth->user('id'),'username'=>$this->Auth->user('username')]);
                    $this->Flash->success(__('The Password has been Changed.'));
                    return $this->redirect($this->referer());
                } else {
                    $this->Flash->error(__('The Password could not be Changed. Please, try again.'));
                    return $this->redirect($this->referer());
                  }
            } else {
                $this->Flash->error(__('The Password could not be Changed. Please enter the correct password and try again.'));
                return $this->redirect($this->referer());
              }
        }
    }
     /**
     * Method for forget password
     * send link on email and reset password
     * 
     */

    public function login1() {

        $this->viewBuilder()->layout('login');
        if ($this->request->is('post')) {
            $query = $this->Users->findByEmail($this->request->data['email']);
            $user = $query->first();
            if (is_null($user)) {
                $this->Flash->error('Email address does not exist. Please try again');
            } else {
                $passkey = uniqid();
                $url = Router::Url(['controller' => 'Users', 'action' => 'reset'], true) . '/' . $passkey;
                
               

                
                
                $timeout = time() + (60 * 60 );
                
                if ($this->Users->updateAll(['passkey' => $passkey, 'timeout' => $timeout], ['id' => $user->id])) {
                   $this->sendResetEmail($url, $user);
                   $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error('Error saving reset passkey/timeout');
                }
            }
        }
    }

    private function sendResetEmail($url, $user) {
	    $email = new Email();
        $email->template('resetpw');
        $email->emailFormat('both');
        $email->from('alerts@dev7.netgen.in');
        $email->to($user->email);
        $email->subject('Reset your password');
        $email->viewVars(['url' => $url, 'username' => $user->username]);
        if ($email->send()) {
			//Log::write('info', 'User password resest link mailed successfully',['user_id'=>$user->id,'username'=>$user->username]);
            $this->Flash->success(__('Check your email for your reset password link'));
        } else {
            $this->Flash->error(__('Error sending email: '));
        }
    }

    public function reset($passkey = null) {
		//debug($passkey);die;
	   $this->viewBuilder()->layout('login');
	   if ($passkey) {
		   $user=$this->Users->find()->where(['passkey' => $passkey])->first();
	       if ($user) {
               if(strtotime("now")<=strtotime($user->timeout)) 
                {
		          if (!empty($this->request->data)) {
					   if($this->request->data['password'] == $this->request->data['confirm_password'])
					   {
                    // Clear passkey and timeout
                          $this->request->data['passkey'] = null;
                          $this->request->data['timeout'] = null;
                          $user = $this->Users->patchEntity($user, $this->request->data);
                          if ($this->Users->save($user)) {
                            $this->Flash->set(__('Your password has been updated.'));
                            return $this->redirect(array('action' => 'login'));
                          } else {
                            $this->Flash->error(__('The password could not be updated. Please, try again.'));
                           }
                        }   else
                          $this->Flash->error(__('The password you have entered does not match !'));           
                  }
                    unset($user->password);
                    $this->set(compact('user'));
		      } 
         } else {
			     $this->Flash->warning('The Link has been expired.Please  try again.');
				//  $this->Flash->set(__('Invalid or expired passkey. Please  try again.'));
			     $this->redirect(['action' => 'login1']);
               }
      
      } else {
            $this->redirect('/');
        }
  }

  /**
  * getManagers method
  * Create datatble displaying all users
  *
  */
    public function getManagers() {

		
        $aColumns = array('users.first_name',
            'users.last_name',
            'users.username',
            'users.phone',
            'users.email',
            'roles.role_name',
            'countries.country_name',
            'states.state_name',
            'cities.city_name',
            'users.pincode',
            'users.id',
        );
        $sIndexColumn = " users.id ";
        $sTable = " users ";
        $sJoinTable = 'INNER JOIN cities cities ON cities.id=users.city_id INNER JOIN states states ON states.id=cities.state_id  INNER JOIN countries countries ON countries.id=states.country_id INNER JOIN roles roles ON roles.id=users.role_id';
        $sConditions = 'users.role_id =2';
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        echo json_encode($returnArr);
        die;
    }
   
}
