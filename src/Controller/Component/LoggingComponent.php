<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class LoggingComponent extends Component{
    public function initialize(array $config){
		$this->Logs=TableRegistry::get('Logs');
		
	}
    public function savelog($userID,$type,$message){
      //  $this->loadModel('Logs');
       $log= $this->Logs->newEntity();
       $arr['user_id']=$userID;
       $arr['type']=$type;
       $arr['message']=$message;
       $arr['time']=Time::now();
         $log= $this->Logs->patchEntity($log,$arr);
         $result=$this->Logs->save($log);
       if($result){
           return true;
       }else{
           return false;
       }
    }
}