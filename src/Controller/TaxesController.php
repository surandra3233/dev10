<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Taxes Controller
 *
 * @property \App\Model\Table\TaxesTable $Taxes
 */
class TaxesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('DataTable');
    }

    public function isAuthorized($user = null) {
        $adminArr = array('index', 'index1', 'add', 'saveDetails', 'archive');
        $mangerArr = array();
        $clientArr = array();
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }

    /**index method
     * 
     */
    public function index() {
        $this->viewBuilder()->layout('admin_layout');
    }

    public function index1() {
        $aColumns = array(
            'taxes.tax_value',
            'taxes.description',
            'taxes.archive',
            'taxes.id',
            
        );
        $sIndexColumn = " taxes.id ";
        $sTable = " taxes ";
        $sJoinTable = '';
        $sConditions = '';
        $returnArr = $this->DataTable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join' => $sJoinTable, 'conditions' => $sConditions));
        echo json_encode($returnArr);
        die;
    }

    /**

      /**
     * Add method
     *
     * 
     */
    public function add() {
        $tax = $this->Taxes->newEntity();
        if ($this->request->is('post')) {
            $tax = $this->Taxes->patchEntity($tax, $this->request->data);
            if ($this->Taxes->save($tax)) {
                $this->Flash->success(__('The tax has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tax could not be saved. Please, try again.'));
        }
        $this->set(compact('tax'));
        $this->set('_serialize', ['tax']);
    }

    public function saveDetails() {
        $tax = $this->Taxes->newEntity();
        $tax = $this->Taxes->patchEntity($tax, $this->request->data);
        if ($this->Taxes->save($tax)) {

            $arr['success'] = true;
            $arr['message'] = 'The tax value has been saved.';
        } else {
            $arr['success'] = false;
            $arr['message'] = 'The tax value could not be saved. Please, try again.';
        }
        echo json_encode($arr);
        die;
    }

    public function archive($id) {
	  $tax = $this->Taxes->get($id);
        if($tax->archive==0){
            $tax->archive=1;
        }else{
             $tax->archive=0;
        }
        if ($this->Taxes->save($tax)) {
                $this->Flash->success(__('The tax archive status has been updated.'));

                
            }else{
            $this->Flash->error(__('The tax could not be saved. Please, try again.'));
        
            }
            return $this->redirect(['action' => 'index']);
    }

}
