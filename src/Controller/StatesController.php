<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * States Controller
 *
 * @property \App\Model\Table\StatesTable $States
 */
class StatesController extends AppController {
public function isAuthorized($user = null) {
        $adminArr = array( 'index');
        $mangerArr = array('index');
        $clientArr = array();
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $country = $this->request->query['country'];
        $states = $this->States->find('list')->where(['country_id' => $country]);
        echo json_encode($states);
        die;
    }

}
