<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Cities Controller
 *
 * @property \App\Model\Table\CitiesTable $Cities
 */
class CitiesController extends AppController
{
    public function isAuthorized($user = null) {
        $adminArr = array( 'index');
        $mangerArr = array('index');
        $clientArr = array();
        if (($user['role_id'] == 1) && (in_array($this->request->action, $adminArr))) {
            return true;
        } elseif (($user['role_id'] == 2) && (in_array($this->request->action, $mangerArr))) {
            return true;
        } elseif (in_array($this->request->action, $clientArr)) {
            return true;
        }
        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
       public function index() {
        $state = $this->request->query['state'];
        $cities = $this->Cities->find('list')->where(['state_id' => $state]);
        echo json_encode($cities);
        die;
    }
}
